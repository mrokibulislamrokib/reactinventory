import React from 'react';
import ReactDOM from 'react-dom';
import { render } from 'react-dom';
import { Router, IndexRoute } from 'react-router';
import { BrowserRouter } from 'react-router-dom';
import { HashRouter,Switch,Route,Link} from 'react-router-dom';
//import createBrowserHistory from 'history/createBrowserHistory';


import './index.css';

import App from './App';
import Apps from './Apps';
//import Main from './main';

import Dashboard from './Dashboard//Dashboard';

import ListCustomer    from './Customer/Listcustomer';
import ListEmployeer   from './Employeer/ListEmployee';
import ListSupplier    from './Supplier/ListSupplier';
import ListOrder       from './Order/ListOrder';
import ListProduct     from './Product/ListProduct';
import HistoryPurchase from './Purchase/HistoryPurchase';
import InvoiceSale     from './Sale/InvoiceSale';
import ListTax         from './Tax/ListTax';
import Setting         from './Setting/Setting';
import ProfileSetting  from './Profile/ProfileSetting';
import Login           from './Profile/Login';
import ListMessage     from './Message/ListMessage';
import ListProductCategory      from './Product/ListProductCategory';
import ListProductSubCategory   from './Product/ListProductSubCategory';
import ListProductBarcode       from './Product/ListProductBarcode';
import ListProductDamaged       from './Product/ListProductDamaged';

import ListPayment              from './Report/Listpayment';
import ListCustomerPayment      from './Report/ListCustomerPayment';


import CreateOrder              from './Order/CreateOrder';
import CreateSale               from './Sale/CreateSale';
import CreatePurchase           from './Purchase/CreatePurchase';


/********** Rokib Custom React Class Import Start */

/*
import Dashboard from './Dashboard//Dashboard';

import CreateCustomer           from './Customer/CreateCustomer';
import ListCustomer             from './Customer/ListCustomer';
import EditCustomer             from './Customer/EditCustomer';

import CreateEmployeer          from './Employeer/CreateEmployee';
import ListEmployeer            from './Employeer/ListEmployee';
import EditEmployeer            from './Employeer/EditEmployee';

import CreateOrder              from './Order/CreateOrder';
import ListOrder                from './Order/ListOrder';
import EditOrder             from './Order/EditEmployee';

import CreateProduct             from './Product/CreateProduct';
import ListProduct               from './Product/ListProduct';
import ListProductBarcode        from './Product/ListProductBarcode';
import ListProductCategory       from './Product/ListProductCategory';
import ListProductSubCategory    from './Product/ListProductSubCategory';

import CreatePurchase            from './Purchase/CreatePurchase';
import HistoryPurchase           from './Purchase/HistoryPurchase';

import ProfileSetting           from './Product/ProfileSetting';

import CreateSale               from './Sale/CreateSale';
import InvoiceSale              from './Sale/InvoiceSale';

import Setting                  from './Setting/Setting';

import CreateSupplier          from './Supplier/CreateSupplier';
import ListSupplier            from './Supplier/ListSupplier';
import EditSupplier            from './Supplier/EditSupplier';

import CreateTax               from './Tax/CreateTax';
import ListTax                 from './Tax/ListTax';
import EditTax                 from './Tax/EditTax';
*/


/********** Rokib Custom React Class Import END */

import registerServiceWorker from './registerServiceWorker';


 //ReactDOM.render(<App />, document.getElementById('root'));
 //render(<App />, document.getElementById('root'));
 

 /* 
render(
    <Router history={browserHistory}>
        <Route path="/" component={Main}>
            <IndexRoute component={Dashboard} /> 
        </Route>
    </Router>,
    document.getElementById('root')

    <Switch>
        <Route exact path='/' component={Dashboard}/>
        <Route path='/customer' component={ListCustomer}/>
        <Route path='/employee' component={ListEmployeer}/>
    </Switch>

  
  render((
    <BrowserRouter>
        <Dashboard/>
    </BrowserRouter>  
),document.getElementById('root'));

);  */




render((
    <HashRouter>
        <App/>
    </HashRouter>  
),document.getElementById('root'));



registerServiceWorker();
