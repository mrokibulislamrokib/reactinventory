import React, { Component } from 'react';
//import DamagedProductslist,{getDamagedProduct,addDamagedProduct,updateDamagedProduct,deleteDamagedProduct} from  './damagedproduct';
import productslist,{getProduct,addProduct,updateProduct,deleteProduct} from './product';
import categorieslist,{getCategory,addCategory,updateCategory,deleteCategory} from './category';
import DamagedProductslist,{getSingleDamagedProduct,getDamagedProduct,addDamagedProduct,updateDamagedProduct,deleteDamagedProduct} from './damagedproduct';

class ListProductDamaged extends Component {

    constructor(props) {
        super(props);
        this.state={
            category_name:'',
            damaged_qty:'',
            damaged_note:'',
            damagedproducts:[],
            products:[],
            categories:[]

        }
        this.addProductDamaged=this.addProductDamaged.bind(this);
        this.updateProductDamaged=this.updateProductDamaged.bind(this);
        this.deleteProductDamaged=this.deleteProductDamaged.bind(this);
    }


    addProductDamaged(e){
        e.preventDefault();
        var category_name=this.refs.category_name.value;
        var damaged_qty=this.refs.damaged_qty.value;
        var damaged_note=this.refs.damaged_note.value;

        var damagedproduct={
          //  product_id:category_name,
            quantity:damaged_qty,
            note:damaged_note
        }

        addDamagedProduct(damagedproduct);

       // this.state.damagedproducts.push(damagedproduct);

        console.log('Send this in a POST request:' + category_name);
    }

    updateProductDamaged(){
        var category_name=this.refs.category_name.value;
        var damaged_qty=this.refs.damaged_qty.value;
        var damaged_note=this.refs.damaged_note.value;

        var damagedproduct={
            //product_id:category_name,
              quantity:damaged_qty,
              note:damaged_note
        }

        updateDamagedProduct(damagedproduct);

        console.log('Send this in a POST request:' + category_name);
    }

    deleteProductDamaged(){

    }

    
    componentDidMount(){
        this.state.damagedproducts=DamagedProductslist;

        getDamagedProduct().then(response => {
            this.setState({
                damagedproducts: response
            });
        });

        getProduct().then(response => {
            this.setState({
                products: response
            });
        });

        getCategory().then(response => {
            this.setState({
                categories: response
            });
        });
    }

    render() {
        return (
            <div>        
                <ol className="breadcrumb pull-right">
                    <li><a href="javascript:;">Home</a></li>
                    <li className="active">Damaged Product</li>
                </ol>
                <h1 className="page-header">Damaged Products</h1>
                <div className="row">
                        <a className="btn btn-success" data-toggle="modal" href='#damaged-modal'>
                            <i className="fa fa-plus"> </i> Add Damaged Product</a>
                </div>
                <br/>

                <div className="row">
			    <div className="col-md-12">
                    <div className="panel panel-inverse">
                        <div className="panel-heading">
                            <div className="panel-heading-btn">
                                <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i className="fa fa-expand"></i></a>
                                <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i className="fa fa-repeat"></i></a>
                                <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i className="fa fa-minus"></i></a>
                                <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i className="fa fa-times"></i></a>
                            </div>
                            <h4 className="panel-title">All Damaged Products</h4>
                        </div>
                        <div className="panel-body">
                            <table id="data-table" className="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Code</th>
                                        <th>Name</th>
                                        <th>Category </th>
                                        <th>Purchase Price</th>
                                        <th>Quantity</th>
                                        <th>Notes</th>
                                        <th>Date</th>
                                        <th>options</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    
                                   {
                                            
                                            this.state.damagedproducts.map(function(item, i){
                                            
                                                    return  <tr className="odd gradeX" key={i}>
                                                
                                                            <td> {item.id}   </td>
                                                            <td> {item.code} </td>
                                                            <td> {item.Name}</td>
                                                            <td> {item.Category}</td>
                                                            <td> {item.PurchasePrice}</td>
                                                            <td> {item.SellingPrice}</td>
                                                            <td> {item.InStock}</td>
                                                            <td> 
                                                                <button   className="btn btn-info btn-icon btn-circle btn-sm">
                                                                        <i className="fa fa-info"></i>
                                                                </button>
                                                                
                                                                <button  className="btn btn-success btn-icon btn-circle btn-sm">
                                                                    <i className="fa fa-edit"></i>
                                                                </button>
                                    
                                                                <button   className="btn btn-danger btn-icon btn-circle btn-sm">
                                                                    <i className="fa fa-trash"></i>
                                                                </button>
                                                            </td>

                                                    </tr>
                                    
                                            })
                                   }

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


            <div className="modal fade" id="damaged-modal">
    <div className="modal-dialog">
        <div className="modal-content">
            <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 className="modal-title">Ezzitech</h4>
            </div>

            <div className="modal-body">

                <form onSubmit={this.addProductDamaged}  
                    className="form-horizontal form-bordered" data-parsley-validate="true" encType="multipart/form-data" method="post" acceptCharset="utf-8">

                    <div className="form-group">
                        <label className="control-label col-md-4 col-sm-4">Category</label>

                        <div className="col-md-6 col-sm-6">
                            <select className="form-control selectpicker" data-size="10" data-live-search="true" data-style="btn-white"
                                    ref="category_name" name="product_id">
                                <option value="" selected="">Select Product</option>
                                    {
                                        this.state.products.map(function(item, i){
                                            
                                            return  <option> {item.name}</option>
                                        })
                                    }

                            </select>
                        </div>

                    </div>

                    <div className="form-group">
                        <label className="control-label col-md-4 col-sm-4">Damaged Quantity</label>
                        <div className="col-md-6 col-sm-6">
                            <input className="form-control" type="number" ref="damaged_qty" name="quantity" required="" placeholder="Damaged Product Quantity" />
                        </div>
                    </div>

                    <div className="form-group">
                        <label className="control-label col-md-4 col-sm-4">Notes</label>
                        <div className="col-md-6 col-sm-6">
                            <textarea className="form-control" ref="damaged_note" name="note" rows="3"></textarea>
                        </div>
                    </div>

                    <div className="form-group">
                        <label className="control-label col-md-4 col-sm-4">
                                </label>
                        <div className="col-md-6 col-sm-6">
                            <select className="form-control selectpicker" data-size="10" data-live-search="true" data-style="btn-white" name="check">
                                        <option value="" selected="">Decrease From Stock ? </option>
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                    </select>
                        </div>
                    </div>

                    <div className="form-group">
                        <label className="control-label col-md-4 col-sm-4"></label>
                        <div className="col-md-6 col-sm-6">
                            <button type="submit" className="btn btn-success">Save Damaged Product</button>
                        </div>
                    </div>

                </form>
            </div>
            <div className="modal-footer">
                <a href="javascript:;" className="btn btn-sm btn-white" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>
                
            </div>
        );
    }
}

export default ListProductDamaged;