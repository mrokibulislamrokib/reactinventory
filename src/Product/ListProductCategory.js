import React, { Component } from 'react';
import categorieslist,{getCategory,addCategory,updateCategory,deleteCategory} from './category';

class ListProductCategory extends Component {
    
    constructor(props) {
        super(props);
        this.state={
            category_name:'',
            category_description:'',
            categories:[]
        };
        this.addCategory=this.addCategory.bind(this);
        this.updateCategory=this.updateCategory.bind(this);
        this.deleteCategory=this.deleteCategory.bind(this);
        this.ProductCategorySearch=this.ProductCategorySearch.bind(this);
    }

    componentDidMount(){
       // this.state.categories=categorieslist;
       getCategory().then(response => {
            this.setState({
                categories: response
            });
        });
    }

    componentWillReceiveProps(){

    }

    addCategory(e){
        
        e.preventDefault();
        
        var category_name=this.refs.category_name.value;
        var category_description=this.refs.category_description.value;

        var category={
            category_name:category_name,
            category_description:category_description
        }

        addCategory(category);

       // this.state.categories.push(category);

        console.log('Send this in a POST request:' + category_name);
    }

    updateCategory(e){
        e.preventDefault();
        var category_name=this.refs.category_name.value;
        var category_description=this.refs.category_description.value;
        console.log('Send this in a POST request:' + category_name);
    }

    deleteCategory(){

    }

    ProductCategorySearch(){

    }

    render() {
        return (
        <div>
            <ol className="breadcrumb pull-right">
                <li><a href="javascript:;">Home</a></li>
                <li className="active">Dashboard</li>
            </ol>
            <h1 className="page-header">Dashboard <small>header small text goes here...</small></h1>
            <div className="row">
                <a className="btn btn-success" data-toggle="modal" href='#category-modal'>Add New category</a>
            </div>
            <br/>

            <div className="row">
                <div className="col-md-12">
                    <div className="panel panel-inverse">
                        <div className="panel-heading">
                            <div className="panel-heading-btn">
                                <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i className="fa fa-expand"></i></a>
                                <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i className="fa fa-repeat"></i></a>
                                <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i className="fa fa-minus"></i></a>
                                <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i className="fa fa-times"></i></a>
                            </div>
                            <h4 className="panel-title">Data Table - Default</h4>
                        </div>
                        <div className="panel-body">
                            <table id="data-table" className="table table-striped table-bordered">
                                <thead>
                                    <tr>    
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Options</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        this.state.categories.map(function(item, i){
                                            return  <tr className="odd gradeX" key={i}>
                                                        <td> {item._id} </td>
                                                        <td> {item.name} </td>
                                                        <td> {item.description} </td>
                                                        <td> {} </td>
                                                    </tr>
                                        })
                                    }
                                   
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div className="modal fade" id="category-modal">
    <div className="modal-dialog">
        <div className="modal-content">
            <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 className="modal-title">Modal title</h4>
            </div>
            <div className="modal-body">
        
		        <form onSubmit={this.addCategory} className="form-horizontal form-bordered" encType="application/x-www-form-urlencoded"   method="post" acceptCharset="utf-8">
			
					<div className="form-group">
						<label className="control-label col-md-4 col-sm-4">
							Name		</label>
						<div className="col-md-6 col-sm-6">
							<input className="form-control" type="text" ref="category_name" name="name" required="" placeholder="Name"/>
						</div>
					</div>

				    <div className="form-group">
				        <label className="control-label col-md-4 col-sm-4">
				        	Description        </label>
				        <div className="col-md-6 col-sm-6">
				            <textarea className="form-control" ref="category_description" name="description" placeholder="Category Description" rows="3"></textarea>
				        </div>
				    </div>

					<div className="form-group">
						<label className="control-label col-md-4 col-sm-4"></label>
						<div className="col-md-6 col-sm-6">
							<button type="submit" className="btn btn-success">Save Category</button>
						</div>
					</div>	

				</form>
				                
            </div>
            <div className="modal-footer">
                <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" className="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
        </div>    
        );
    }
}

export default ListProductCategory;