import React, { Component } from 'react';
import supplierslist ,{getSupplier,addSupplier,updateSupplier,deleteSupplier} from './supplier';
import { HashRouter,Switch,Route,Link,NavLink} from 'react-router-dom';
class ListSupplier extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            supplier_company_name:'',
            supplier_name:'',
            supplier_email:'',
            supplier_phone:'',
            supplier_image:'',
            suppliers: []
        };
        this.addSupplier=this.addSupplier.bind(this);
        this.updateSupplier=this.updateSupplier.bind(this);
        this.deleteSupplier=this.deleteSupplier.bind(this);
        this.SupplierSearch=this.SupplierSearch.bind(this); 
    }

    componentDidMount(){
       // this.state.suppliers=supplierslist;
       getSupplier().then(response => {
            this.setState({
                suppliers: response
            });
        });
    }

    componentWillReceiveProps(){

    }

    addSupplier(e){
        e.preventDefault();
        var supplier_company_name=this.refs.supplier_company_name.value;
        var supplier_name=this.refs.supplier_name.value;
        var supplier_email=this.refs.supplier_email.value;
        var supplier_phone=this.refs.supplier_phone.value;

        var supplier={
            supplier_company_name:supplier_company_name,
            supplier_name:supplier_name,
            supplier_email:supplier_email,
            supplier_phone:supplier_phone
        }

        addSupplier(supplier);

     //   this.state.suppliers.push(supplier);
        console.log('Send this in a POST request:' +supplier_name );
    }

    updateSupplier(e){
        e.preventDefault();
        var supplier_company_name=this.refs.supplier_company_name.value;
        var supplier_name=this.refs.supplier_name.value;
        var supplier_email=this.refs.supplier_email.value;
        var supplier_phone=this.refs.supplier_phone.value;
        console.log('Send this in a POST request:' +supplier_name );
    }

    deleteSupplier(e){
        
    }

    SupplierSearch(e){

    }

    render() {
   //     console.log(this.state.suppliers);
        return (
            <div>
                <ol className="breadcrumb pull-right">
                    <li><a href="javascript:;">Home</a></li>
                    <li className="active">Dashboard</li>
                </ol>

                <h1 className="page-header">Dashboard <small>header small text goes here...</small></h1>
    
                <div className="row">
                        <a className="btn btn-success" data-toggle="modal" href='#supplier-modal'>Add New supplier</a>
                </div>

                <br/>

                    <div className="row">
                        <div className="col-md-12">
                            <div className="panel panel-inverse">
                                <div className="panel-heading">
                                    <div className="panel-heading-btn">
                                        <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i className="fa fa-expand"></i></a>
                                        <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i className="fa fa-repeat"></i></a>
                                        <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i className="fa fa-minus"></i></a>
                                        <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i className="fa fa-times"></i></a>
                                    </div>
                                    <h4 className="panel-title">Data Table - Default</h4>
                                </div>
                                <div className="panel-body">
                                    <table id="data-table" className="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Company</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th>Options</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                    {
                                        this.state.suppliers.map(function(item, i){
                                            
                                            return  <tr className="odd gradeX" key={i}>
                                                            <td> { item._id } </td>
                                                            <td> { item.company_name } </td>
                                                            <td> { item.supplier_name }</td>
                                                            <td> { item.email }</td>
                                                            <td> { item.phone }</td>
                                                            <td> 
                                                            <Link  to={`/supplieredit/${item._id}`} className="btn btn-info btn-icon btn-circle btn-sm"> <i className="fa fa-edit"></i></Link>
                                                            <Link to={`/supplierdelete/${item._id}`}  className="btn btn-success btn-icon btn-circle btn-sm"> <i className="fa fa-trash"></i></Link>

                                                                <button  className="btn btn-info btn-icon btn-circle btn-sm">
                                                                    <i className="fa fa-info"></i>
                                                                </button>
                                                            </td>
                                                    </tr>

                                                    
                                        })
                                    }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div className="modal fade" id="supplier-modal">
    <div className="modal-dialog">
        <div className="modal-content">
            <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 className="modal-title">Modal title</h4>
            </div>
            <div className="modal-body">
                <form onSubmit={this.addSupplier} className="form-horizontal form-bordered" encType="application/x-www-form-urlencoded">

				    <div className="form-group">
				        <label className="control-label col-md-4 col-sm-4">
				            Company Name </label>
				        <div className="col-md-6 col-sm-6">
				            <input className="form-control" type="text" ref="supplier_company_name" name="company" placeholder="Supplier Company"/>
				        </div>
				    </div>

				    <div className="form-group">
				        <label className="control-label col-md-4 col-sm-4">
				            Supplier Name </label>
				        <div className="col-md-6 col-sm-6">
				            <input className="form-control" type="text" ref="supplier_name" name="name" required="" placeholder="Supplier Name"/>
				        </div>
				    </div>

				    <div className="form-group">
				        <label className="control-label col-md-4 col-sm-4">
				            Email </label>
				        <div className="col-md-6 col-sm-6">
				            <input className="form-control" type="email" ref="supplier_email" name="email" required="" placeholder="Supplier Email"/>
				        </div>
				    </div>

				    <div className="form-group">
				        <label className="control-label col-md-4 col-sm-4">
				            Phone </label>
				        <div className="col-md-6 col-sm-6">
				            <input className="form-control" type="text" ref="supplier_phone" name="phone" placeholder="Supplier Phone Number"/>
				        </div>
				    </div>

				    <div className="form-group">
				        <label className="control-label col-md-4 col-sm-4">
				            Image </label>
				        <div className="col-md-6 col-sm-6">
				            <input className="form-control" type="file" ref="supplier_image" name="userfile" placeholder="Supplier Image"/>
				        </div>
				    </div>

				    <div className="form-group">
				        <label className="control-label col-md-4 col-sm-4"></label>
				        <div className="col-md-6 col-sm-6">
				            <button type="submit" className="btn btn-success">Save Supplier</button>
				        </div>
				    </div>

				</form>
				                
            </div>
            <div className="modal-footer">
                <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" className="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
                </div>
        );
    }
}

export default ListSupplier;