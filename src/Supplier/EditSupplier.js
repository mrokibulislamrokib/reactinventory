import React, { Component } from 'react';
import supplierslist ,{getSingleSupplier,getSupplier,addSupplier,updateSupplier,deleteSupplier} from './supplier';
import supplier from './supplier';
class EditSupplier extends Component {

    constructor(props) {
        super(props);
        this.state={
            supplier_company_name:'',
            supplier_name:'',
            supplier_email:'',
            supplier_phone:'',
            supplier:[]
        }
    }

    componentDidMount() {
        var postno= this.props.match.params.id;
        console.log(postno);
        getSupplier(postno).then(response => {
            console.log(response[0].company_name)
            this.setState({
               supplier_company_name:response[0].company_name,
                supplier_name:response[0].supplier_name,
                supplier_email:response[0].email,
                supplier_phone:response[0].phone 
            });
        });
    }
    

    render() {
        return (
            <div>

<form  className="form-horizontal form-bordered" encType="application/x-www-form-urlencoded">

<div className="form-group">
    <label className="control-label col-md-4 col-sm-4">
        Company Name </label>
    <div className="col-md-6 col-sm-6">
        <input className="form-control" type="text" value={this.state.supplier_company_name} name="company" placeholder="Supplier Company"/>
    </div>
</div>

<div className="form-group">
    <label className="control-label col-md-4 col-sm-4">
        Supplier Name </label>
    <div className="col-md-6 col-sm-6">
        <input className="form-control" type="text" value={this.state.supplier_name} name="name" required="" placeholder="Supplier Name"/>
    </div>
</div>

<div className="form-group">
    <label className="control-label col-md-4 col-sm-4">
        Email </label>
    <div className="col-md-6 col-sm-6">
        <input className="form-control" type="email"  value={this.state.supplier_email} name="email" required="" placeholder="Supplier Email"/>
    </div>
</div>

<div className="form-group">
    <label className="control-label col-md-4 col-sm-4">
        Phone </label>
    <div className="col-md-6 col-sm-6">
        <input className="form-control" type="text"  value={this.state.supplier_phone} name="phone" placeholder="Supplier Phone Number"/>
    </div>
</div>

<div className="form-group">
    <label className="control-label col-md-4 col-sm-4">
        Image </label>
    <div className="col-md-6 col-sm-6">
        <input className="form-control" type="file" ref="supplier_image" name="userfile" placeholder="Supplier Image"/>
    </div>
</div>

<div className="form-group">
    <label className="control-label col-md-4 col-sm-4"></label>
    <div className="col-md-6 col-sm-6">
        <button type="submit" className="btn btn-success">Save Supplier</button>
    </div>
</div>

</form>
            </div>
        );
    }
}

export default EditSupplier;