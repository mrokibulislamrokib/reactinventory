import React, { Component } from 'react';
import orderslist,{getSingleOrder,getOrder,addOrder,updateOrder,deleteOrder} from './order';
import {getCustomer} from '../Customer/customer';
import {getProduct} from '../Product/product';
class CreateOrder extends Component {

    constructor(props){
        super(props);
        this.state={
			order_number:'',
			order_customer:'',
			order_creating_timestamp:'',
			order_status:'',
			order_payment_status:'',
			order_shipping_address:'',
			order_note:'',
			order_sub_total:'',
			order_vat_percentage:'',
			order_grand_total:'',
			order_payment:'',
			order_change_amount:'',
			order_net_payment:'',
			order_due_amount:'',
			order_payment_method:'',
			orders:[],
			customers:[],
			products:[]
		}
		this.addOrder=this.addOrder.bind(this);
		this.calculate_grand_total=this.calculate_grand_total.bind(this);
		this.calculate_change_amount=this.calculate_change_amount.bind(this);
	}
	
	componentDidMount(){
		
		getCustomer().then(response => {
			this.setState({
				customers: response
			});
		});

		getProduct().then(response => {
            this.setState({
                products: response
            });
        });
	}

    addOrder(e){
        e.preventDefault();
		var order_number=this.refs.order_number.value;
		var order_customer=this.refs.order_customer.value;
		var order_creating_timestamp=this.refs.order_creating_timestamp.value;
		var order_status=this.refs.order_status.value;
		var order_payment_status=this.refs.order_payment_status.value;
		var order_shipping_address=this.refs.order_shipping_address.value;
		var order_note=this.refs.order_note.value;
		var order_sub_total=this.refs.order_sub_total.value;
		var order_vat_percentage=this.refs.order_vat_percentage.value;
		var order_grand_total=this.refs.order_vat_percentage.value;
		var order_payment=this.refs.order_payment.value;
		var order_change_amount=this.refs.order_change_amount.value;
		var order_net_payment=this.refs.order_net_payment.value;
		var order_due_amount=this.refs.order_due_amount.value;
		var order_payment_method=this.refs.order_payment_method.value;

		var order={
			order_number:order_number,
			order_customer:order_customer,
			order_creating_timestamp:order_creating_timestamp,
			order_status:order_status,
			order_payment_status:order_payment_status,
			order_shipping_address:order_shipping_address,
			order_note:order_note,
			order_sub_total:order_sub_total,
			order_vat_percentage:order_vat_percentage,
			order_grand_total:order_grand_total,
			order_payment:order_payment,
			order_change_amount:order_change_amount,
			order_net_payment:order_net_payment,
			order_due_amount:order_due_amount,
			order_payment_method:order_payment_method
		}

		addOrder(order);

	//	this.state.orders.push(order);
		
		console.log('Send this in a POST request:' + order_number);
	}
	
	calculate_grand_total(){

	}

	calculate_change_amount(){

	}

    render() {
        return (
<div> 
    <form onSubmit={this.addOrder} className="form-horizontal" data-parsley-validate="true" encType="multipart/form-data" method="post" acceptCharset="utf-8">
	<div className="row">
		<div className="col-md-12">
	        <div className="panel panel-inverse">
                <div className="panel-heading">
                    <div className="panel-heading-btn">
                        <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand">
                        	<i className="fa fa-expand"></i>
                        </a>
                        <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse">
                        	<i className="fa fa-minus"></i>
                        </a>
                    </div>
                    <h4 className="panel-title">Basic Information</h4>
                </div>

                <div className="panel-body">
                    <div className="col-md-6">
                    	<div className="note note-success">
							<h4>Instructions</h4>
							<ul>
								<li>Check and recheck added products before creating the order. Products added can not be edited later but other informations are editable.</li>
								<li>You can add multiple products in a single order.</li>
								<li>If order status and payment status is not selected, they will be automatically counted as pending order with unpaid payment status.</li>
								<li>The number in parenthesis in product selector represents the present stock quantity of the product.</li>
							</ul>
						</div>
                    </div>


                    <div className="col-md-6">

						<div className="form-group">
                            <label className="control-label col-md-3 col-sm-3">Order Code</label>
                            <div className="col-md-9 col-sm-9">
                                <input className="form-control" ref="order_number" type="text" name="order_number" />
                            </div>
                        </div>

                        <div className="form-group">
							<label className="control-label col-md-3 col-sm-3">Customer</label>
							<div className="col-md-9 col-sm-9">
							    <select className="form-control selectpicker" ref="order_customer">
					                <option value="" selected>Select Customer</option>
					                {
										this.state.customers.map(function(item, i){
											<option value="">{item.name}</option>
										})
									}		            
					    		</select>
							</div>
						</div>

						<div className="form-group">
                            <label className="control-label col-md-3 col-sm-3">Date</label>
                            <div className="col-md-9 col-sm-9">
                                <input type="text" className="form-control" id="datepicker-autoClose" ref="order_creating_timestamp" name="creating_timestamp"
                                	placeholder="Select Date" defaultValue="12/20/2017"/>
                            </div>
                        </div>

					</div>
                </div> 
            </div> 
        </div>
    </div>

    <div id="order_info">
		<div className="row">
			<div className="col-md-12">
		        <div className="panel panel-inverse">
	                <div className="panel-heading">
	                    <div className="panel-heading-btn">
	                        <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i className="fa fa-expand"></i></a>
	                        <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i className="fa fa-minus"></i></a>
	                    </div>
	                    <h4 className="panel-title">Purchase Products</h4>
	                </div>

		            <div className="panel-body">
	                    <div className="col-md-3">
	                    	<div className="form-group">
								<div className="col-md-12 col-sm-12">
								    <select className="form-control selectpicker" name="product_id">
						                <option value="" selected>Add A Product</option>
						                {
											this.state.products.map(function(item, i){
												<option value="">{item.name}</option>
											})
										}
						       		</select>
								</div>
							</div>
	                    </div>

	                    <div className="col-md-9">
	                    	<div className="table-responsive">
								<table className="table table-bordered">
									<thead>
										<tr>
											<th>#</th>
											<th>Serial</th>
											<th>Name</th>
											<th>Quantity</th>
											<th>Unit Price</th>
											<th>Total</th>
											<th><i className="fa fa-trash"></i></th>
										</tr>
									</thead>
									<tbody id="order_entry_holder">

									</tbody>
								</table>
							</div>
	                    </div>
		            </div>
	            </div>
		    </div>
		</div>


		<div className="row">
			<div className="col-md-7">
	            <div className="panel panel-default">
	                <div className="panel-heading">
	                    <div className="panel-heading-btn">
	                        <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-default">
	                        	<i className="fa fa-expand"></i>
	                        </a>
	                        <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-warning">
	                        	<i className="fa fa-minus"></i>
	                        </a>
	                    </div>

	                    <h4 className="panel-title"> Other Informations</h4>
	                </div>

	                <div className="panel-body">

	                    <div className="form-group">
							<label className="control-label col-md-3 col-sm-3">Order Status</label>
							<div className="col-md-9 col-sm-9">
								<select className="form-control selectpicker" data-size="10" data-live-search="true" data-style="btn-white" data-parsley-required="true"
										ref="order_status" name="order_status">
					                <option value="" selected>Select Order Status</option>
					                <option value="0">Pending</option>
					                <option value="1">Approved</option>
					                <option value="2">Rejected</option>
					            </select>
							</div>
						</div>

						<div className="form-group">
							<label className="control-label col-md-3 col-sm-3">Payment Status</label>
							<div className="col-md-9 col-sm-9">
								<select className="form-control selectpicker" data-size="10" data-live-search="true" data-style="btn-white" data-parsley-required="true" 
									ref="order_payment_status" name="payment_status">
					                <option value="" selected>Select Payment Status</option>
					                <option value="0">Unpaid</option>
					                <option value="1">Paid</option>
					            </select>
							</div>
						</div>

						<div className="form-group">
					        <label className="control-label col-md-3 col-sm-3">Address</label>
					        <div className="col-md-9 col-sm-9">
					            <textarea className="form-control" ref="order_shipping_address" name="shipping_address" placeholder="Shipping Address" rows="3"></textarea>
					        </div>
					    </div>

					    <div className="form-group">
					        <label className="control-label col-md-3 col-sm-3">Note</label>
					        <div className="col-md-9 col-sm-9">
					            <textarea className="form-control" ref="order_note" name="note" placeholder="Notes" rows="3"></textarea>
					        </div>
					    </div>

	                </div>
	            </div>
	        </div>

	        <div className="col-md-5">
	            <div className="panel panel-default" data-sortable-id="ui-widget-10">
	                <div className="panel-heading">
	                    <div className="panel-heading-btn">
	                        <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i className="fa fa-expand"></i></a>
	                        <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i className="fa fa-minus"></i></a>
	                    </div>
	                    <h4 className="panel-title">Payment</h4>
	                </div>
	                <div className="panel-body">
	                    <div className="table-responsive">
							<table className="table table-bordered">
								<tbody>
									<tr>
										<td>Sub Total</td>
										<td>
											<input type="text" className="form-control text-right" id="sub_total" value="" ref="order_sub_total" name="sub_total"/>
										</td>
									</tr>
									<tr>
										<td>Discount</td>
										<td id="customer_discount_holder"></td>
									</tr>
									<tr>
										<td>VAT</td>
										<td>
											<input className="form-control text-right" type="text" ref="order_vat_percentage" name="vat_percentage" id="vat_percentage"
											 onKeyUp={this.calculate_grand_total}
												value="" placeholder="VAT Percentage" data-parsley-required="true"/>
										</td>
									</tr>
									<tr>
										<td>Grand Total</td>
										<td>
											<input type="text" className="form-control text-right" id="grand_total" value="" ref="order_grand_total" name="grand_total"/>
										</td>
									</tr>
									<tr>
										<td>Payment</td>
										<td>
											<input className="form-control text-right" type="text" ref="order_payment" name="" id="payment" value=""
												onKeyUp={this.calculate_change_amount}
													placeholder="Enter Payment Amount"
														data-parsley-required="true"/>
										</td>
									</tr>
									<tr>
											<td>Change</td>
											<td>
												<input type="text" className="form-control text-right" value="" ref="order_change_amount" id="change_amount"/>
											</td>
										</tr>
										<tr>
											<td>Net Payment</td>
											<td>
												<input type="text" className="form-control text-right" value="" id="net_payment" ref="order_net_payment" name="amount"/>
											</td>
										</tr>
										<tr>
											<td>Due</td>
											<td>
												<input type="text" className="form-control text-right" value="" id="due_amount" ref="order_due_amount" name="due"/>
											</td>
										</tr>
										<tr>
											<td>Method</td>
											<td>
												<select className="form-control" ref="order_payment_method" name="method" data-parsley-required="true">
									                <option value="" selected>Select Payment Method</option>
									                <option value="1">Cash</option>
									                <option value="2">Check</option>
									                <option value="3">Card</option>
									            </select>
											</td>
										</tr>
								</tbody>
							</table>
							<div className="form-group col-md-10">
								<button type="submit" className="btn btn-success">Create New Order</button>
							</div>
						</div>
	                </div>
	            </div>
			</div>
	    </div>
	</div>
</form>

 </div>
        );
    }
}

export default CreateOrder;