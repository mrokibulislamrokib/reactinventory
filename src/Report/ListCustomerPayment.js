import React, { Component } from 'react';
import customerpaymentslist from './customerpayment';

class ListCustomerPayment extends Component {

	constructor(props){
        super(props);
        this.state={
            start_date:'',
            end_date:'',
            customerpayments:customerpaymentslist
        }
        this.paymentCustomerReportSubmit=this.paymentCustomerReportSubmit.bind(this);
    }

    paymentCustomerReportSubmit(e){
        e.preventDefault();
        var start_date=this.refs.start_date.value;
        var end_date=this.refs.end_date.value;
        console.log('Send this in a POST request:' + start_date);
	}
	
    render() {
        return (
            <div>

<div className="row">
	<div className="col-md-3"></div>
		<div className="col-md-6">
	        <div className="panel panel-inverse" data-sortable-id="ui-widget-1">
                <div className="panel-heading">
                    <div className="panel-heading-btn">
                        <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i className="fa fa-expand"></i></a>
                        <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i className="fa fa-minus"></i></a>
                    </div>
                    <h4 className="panel-title">Select Date Range</h4>
                </div>

                <div className="panel-body">
                    <form onSubmit={this.paymentCustomerReportSubmit} className="form-horizontal form-bordered" data-parsley-validate="true" encType="multipart/form-data" method="post" acceptCharset="utf-8">

						<div className="form-group">
						    <label className="col-md-3">Date Range</label>
						    <div className="col-md-8">
						        <div className="input-group input-daterange">
						            <input type="text" className="form-control" ref="start_date" name="start"
						            	defaultValue="11/21/2017" />
						            <span className="input-group-addon">To</span>
						            <input type="text" className="form-control" ref="end_date" name="end"
						            	defaultValue="12/20/2017" />
						        </div>
						    </div>
						</div>

						<div className="form-group">
							<label className="col-md-3 col-sm-3"></label>
							<div className="col-md-8 col-sm-8">
								<button type="submit" className="btn btn-success">Go</button>
							</div>
						</div>

					</form>                
				</div>
            </div>
		</div>
		<div className="col-md-3">
			<div className="alert alert-info fade in m-b-15">
				<strong>
					21 Nov, 2017 - 20 Dec, 2017				</strong>
			</div>
		</div>
	</div>
	
	<div className="row">
		<div className="col-md-12">
	        <div className="widget-chart bg-black">
	            <div className="">
	                <h4 className="chart-title">
	                    
	                </h4>
	                <div id="chartdiv"></div>
	            </div>
	        </div>
	    </div>
	</div>

	<div className="row">
		<div className="col-md-12">
            <div className="panel panel-inverse">
                <div className="panel-heading">
                    <div className="panel-heading-btn">
                        <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i className="fa fa-expand"></i></a>
                        <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i className="fa fa-minus"></i></a>
                    </div>
                    <h4 className="panel-title">Customer Payments</h4>
                </div>
                <div className="panel-body">
                    <div className="table-responsive">
                        
                        <table id="data-table" className="table table-striped table-bordered nowrap display" width="100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Date</th>
                                    <th>Amount</th>
                                    <th>Customer</th>
                                    <th>Type</th>
                                    <th>Method</th>
                                </tr>
                            </thead>
                            <tbody>

                            {
                                            
                                            this.state.customerpayments.map(function(item, i){
                                            
                                                    return  <tr className="odd gradeX" key={i}>
                                                
                                                            <td> {item.id}   </td>
                                                            <td> {item.date} </td>
                                                            <td> {item.amount}</td>
                                                            <td> {item.customer} </td>
                                                            <td> {item.type}</td>
                                                            <td> {item.method}</td>
                                                    </tr>
                                    
                                            })
                                   }
                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
		</div>
	</div>
          
            </div>
        );
    }
}

export default ListCustomerPayment;