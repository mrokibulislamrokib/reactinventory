import React, { Component } from 'react';
import customerslist, {getSingleCustomer,getCustomer,addCustomer,updateCustomer,deleteCustomer} from './customer';
class CustomerPost extends Component {

    constructor(props) {
        super(props);
        this.state={
            editing_id:'',
            deleteing_id:''
        };
    }

    componentDidMount() {

    }

    editCustomer(id,e){
        this.setState({
            editing_id:id
        });
    }

    deleteCustomer(id,e){
        this.setState({
            deleteing_id:id
        });
        console.log('deleting customer' + this.state.deleteing_id);
    }

    render() {
        var {customer_code,customer_name,email,address,phone,_id}=this.props;
        var rid={_id};
        return (
                <tr className ="odd gradeX">
                        <td> {_id} </td>
                        <td> {customer_code} </td>
                        <td> {customer_name}</td>
                        <td> {email}</td>
                        <td> {address}</td>
                        <td> {phone}</td>
                        <td> 
                        
                            <button onClick={this.editCustomer.bind(this,rid._id)} className="btn btn-info btn-icon btn-circle btn-sm">
                                    <i className="fa fa-info"></i>
                            </button>

                            <button onClick={this.deleteCustomer.bind(this,rid._id)} className="btn btn-danger btn-icon btn-circle btn-sm">
                                 <i className="fa fa-trash"></i>
                            </button>

                          
                        </td>
                </tr>
        );
    }
}

export default CustomerPost;