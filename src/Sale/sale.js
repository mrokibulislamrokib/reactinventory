import axios from 'axios';

export function getSingleSale(){
    var request_url='http://localhost:7000/api/invoice/';
    return axios.get(request_url,{
       // _id:id
    }).then(function(res){
        if(res.status!==200){
        //    throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
      //  throw new Error(error.statusText);
    });
}


export function getSale(){
    var request_url='http://localhost:7000/api/invoice/';
    return axios.get(request_url,{
       // _id:id
    }).then(function(res){
        if(res.status!==200){
        //    throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
       // throw new Error(error.statusText);
    });
}

export function addSale(Invoice){
    var request_url='http://localhost:7000/api/invoice/';
    return axios.post(request_url,{
        invoice_code:Invoice.sale_invoice_code,
      //  invoice_entries:Invoice.invoice_entries,
     //   discount_percentage:Invoice.discount_percentage,
        vat_percentage:Invoice.sale_tax_percentage,
        sub_total:Invoice.sale_sub_total,
        grand_total:Invoice.sale_grand_total,
        due:Invoice.sale_due_amount,
       // customer_id:Invoice.sale_customer_id,
       // timestamp:Invoice.sale_timestamp
    }).then(function(res){
        if(res.status!==200){
           // throw new Error(res.statusText);
        }else{
            console.log(res.data)
            //return res.data;
        }
    }).catch(function(error){
       // throw new Error(error.statusText);
    });
}


export function updateSale(id,name){
    var request_url='http://localhost:7000/api/invoice/';
    return axios.put(request_url,{
        /*
        _id:id,
        invoice_code:invoice_code,
        invoice_entries:invoice_entries,
        discount_percentage:discount_percentage,
        vat_percentage:vat_percentage,
        sub_total:sub_total,
        grand_total:grand_total,
        due:due,
        customer_id:customer_id,
        timestamp:timestamp
        */
    }).then(function(res){
        if(res.status!==200){
            throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
        throw new Error(error.statusText);
    });
}

export function deleteSale(id){
    var request_url='http://localhost:7000/api/invoice/';
    return axios.post(request_url,{
        //_id:id
    }).then(function(res){
        if(res.status!==200){
            throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
        throw new Error(error.statusText);
    });
}


const Saleslist=
    [
        {id:1,Salecode:"#5665",customer:"John",email:"dvf",date_created:"350",last_modified:"456"},
    ];

export default Saleslist;