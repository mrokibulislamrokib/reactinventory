import React, { Component } from 'react';
import purchaselist,{getSinglePurchase,getPurchase,addPurchase,updatePurchase,deletePurchase} from './purchase';
import {getSupplier} from '../Supplier/supplier';
import {getProduct} from '../Product/product';
class CreatePurchase extends Component {
    
    constructor(props){
        super(props);
        this.state={
            purchase_code:'',
            supplier_id:'',
            purchase_timestamp:'',
            purchase_grand_total:'',
            purchase_amount:'',
            purchase_method:'',
            purchases:[],
            products:[],
            suppliers:[]
        }
        this.addPurchase=this.addPurchase.bind(this);
        this.add_product_for_purchase=this.add_product_for_purchase.bind(this);
    }

    componentDidMount(){

        getProduct().then(response => {
            this.setState({
                products: response
            });
        });
    }

    addPurchase(e){
        e.preventDefault();
        var purchase_code=this.refs.purchase_code.value;
        var supplier_id=this.refs.supplier_id.value;
        var purchase_timestamp=this.refs.purchase_timestamp.value;
        var purchase_grand_total=this.refs.purchase_grand_total.value;
        var purchase_amount=this.refs.purchase_amount.value;
        var purchase_method=this.refs.purchase_method.value;

        var purchase={
            purchase_code:purchase_code,
            supplier_id:supplier_id,
            purchase_timestamp:purchase_timestamp,
            purchase_grand_total:purchase_grand_total,
            purchase_amount:purchase_amount,
            purchase_method:purchase_method
        }

        addPurchase(purchase);

      //  this.state.purchases.push(purchase);

        console.log('Send this in a POST request:' + supplier_id);
    }




    add_product_for_purchase(){

    }

    render() {
        return (
            <div>
                <form  onSubmit={this.addPurchase} className="form-horizontal" data-parsley-validate="true" encType="multipart/form-data" 	method="post" acceptCharset="utf-8">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="panel panel-inverse">
                                <div className="panel-heading">
                                    <div className="panel-heading-btn">
                                        <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i className="fa fa-expand"></i></a>
                                        <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i className="fa fa-minus"></i></a>
                                    </div>
                                    <h4 className="panel-title">Basic Information</h4>
                                </div>
                                <div className="panel-body">
                                    <div className="col-md-6">
                                        <div className="note note-success">
                                            <h4>Instructions</h4>
                                            <ul>
                                                <li>Check and recheck the informations you have given before creating the purchase. The purchase once made can not be altered.</li>
                                                <li>You can add multiple products of any amount during the purchase.</li>
                                                <li>The number in parenthesis in product selector represents the present stock quantity of the product.</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div className="col-md-6">

                                        <div className="form-group">
                                            <label className="control-label col-md-3 col-sm-3">Purchase Code </label>
                                            <div className="col-md-9 col-sm-9">
                                                <input className="form-control" type="text" ref="purchase_code" ref="purchase_code" name="purchase_code" data-parsley-required="true"
                                                    defaultValue="965f8acb4f" readOnly />
                                            </div>
                                        </div>

                                        <div className="form-group">
                                            <label className="control-label col-md-3 col-sm-3"> Supplier</label>
                                            <div className="col-md-9 col-sm-9">
                                                <select className="form-control selectpicker" ref="supplier_id" data-size="10" data-live-search="true" data-style="btn-white" data-parsley-required="true" name="supplier_id">
                                                    <option value="" selected>Select Supplier</option>
                                                    {
                                                        this.state.suppliers.map(function(item,i){
                                                            <option value=""> {item.name} </option>
                                                        })
                                                    }
                                                   
                                                </select>
                                            </div>
                                        </div>

                                        <div className="form-group">
                                            <label className="control-label col-md-3 col-sm-3">
                                                Date                            </label>
                                            <div className="col-md-9 col-sm-9">
                                                <input type="text" className="form-control" id="datepicker-autoClose" name="timestamp"
                                                   ref="purchase_timestamp"  placeholder="Select Date" defaultValue="12/20/2017"/>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-md-12">
                            <div className="panel panel-inverse">
                                <div className="panel-heading">
                                    <div className="panel-heading-btn">
                                        <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i className="fa fa-expand"></i></a>
                                        <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i className="fa fa-minus"></i></a>
                                    </div>
                                    <h4 className="panel-title">Purchase Products</h4>
                                </div>
                                <div className="panel-body">
                                    <div className="col-md-3">

                                        <div className="form-group">
                                            <div className="col-md-12 col-sm-12">
                                                <select onChange={this.add_product_for_purchase} className="form-control selectpicker" data-size="10" data-live-search="true" data-style="btn-success" name="product_id">
                                                    <option value="" selected>Add A Product</option>
                                                    {
                                            this.state.products.map(function(item,i){

                                             return <option value="">{item.name}  </option>

                                            })
                                        }
                                                </select>
                                            </div>
                                        </div>

                                    </div>

                                    <div className="col-md-9">
                                        <div className="table-responsive">
                                            <table className="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Serial</th>
                                                        <th>Name</th>
                                                        <th>Quantity</th>
                                                        <th>Unit Price</th>
                                                        <th>Total</th>
                                                        <th><i className="fa fa-trash"></i></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="purchase_entry_holder">

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-md-7"></div>
                        <div className="col-md-5">
                            <div className="panel panel-default" data-sortable-id="ui-widget-10">
                                <div className="panel-heading">
                                    <div className="panel-heading-btn">
                                        <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i className="fa fa-expand"></i></a>
                                        <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i className="fa fa-minus"></i></a>
                                    </div>
                                    <h4 className="panel-title">Payment</h4>
                                </div>
                                <div className="panel-body">
                                    <div className="table-responsive">
                                        <table className="table table-bordered">
                                            <tbody>
                                                <tr>
                                                    <td>Grand Total</td>
                                                    <td className="text-right" ref="purchase_grand_total" id="grand_total"></td>
                                                </tr>
                                                <tr>
                                                    <td>Payment</td>
                                                    <td>
                                                        <input className="form-control" type="text" ref="purchase_amount" name="amount" id="" defaultValue="" placeholder="Enter Payment Amount"
                                                            data-parsley-required="true"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Method</td>
                                                    <td>
                                                        <select className="form-control" ref="purchase_method" name="method">
                                                            <option value="" selected>Select Payment Method</option>
                                                            <option value="1">Cash</option>
                                                            <option value="2">Check</option>
                                                            <option value="3">Card</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <div className="form-group col-md-10">
                                            <button type="submit" className="btn btn-success">Create New Purchase</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        );
    }

}

export default CreatePurchase;