import React, { Component } from 'react';
import axios from 'axios';
import productslist,{getProduct,addProduct,updateProduct,deleteProduct} from './product';
import categorieslist,{getCategory,addCategory,updateCategory,deleteCategory} from './category';
import subcategorieslist,{getSubCategory,addSubCategory,updateSubCategory,deleteSubCategory} from './subcategory';
class ListProduct extends Component {

    constructor(props) {
        
        super(props);

        this.state = {
            product_code:'',
            product_name:'',
            product_category:'',
            product_sub_category:'',
            product_purchase_price:'',
            product_selling_price:'',
            product_notes:'',
            product_image:'',
            products: [],
            categories:[],
            subcategories:[]
        };

        this.addProduct=this.addProduct.bind(this);
        this.updateProduct=this.updateProduct.bind(this);
        this.deleteProduct=this.deleteProduct.bind(this);
        this.ProductSearch=this.ProductSearch.bind(this);
    }

    componentDidMount(){
       // this.state.products=productslist;
        getProduct().then(response => {
            this.setState({
                products: response
            });
        });

        getCategory().then(response => {
            this.setState({
                categories: response
            });
        });

      /*  getSubCategory().then(response => {
            this.setState({
                subcategories: response
            });
        }); */

    }

    componentWillReceiveProps(){

    }

    addProduct(e){
        e.preventDefault();
        var product_code=this.refs.product_code.value;
        var product_name=this.refs.product_name.value;
        var product_category=this.refs.product_category.value;
        var product_sub_category=this.refs.product_sub_category.value;
        var product_purchase_price=this.refs.product_purchase_price.value;
        var product_selling_price=this.refs.product_selling_price.value;
        var product_notes=this.refs.product_notes.value;

        var product={
            product_code:product_code,
            product_name:product_name,
            product_category:product_category,
            product_sub_category:product_sub_category,
            product_purchase_price:product_purchase_price,
            product_selling_price:product_selling_price,
            product_notes:product_notes
        }

        addProduct(product);
        
        //this.state.products.push(product);
        
        console.log('Send this in a POST request:' + product_name);
    }

    updateProduct(e){
        e.preventDefault();
        var product_code=this.refs.product_code.value;
        var product_name=this.refs.product_name.value;
        var product_category=this.refs.product_category.value;
        var product_sub_category=this.refs.product_sub_category.value;
        var product_purchase_price=this.refs.product_purchase_price.value;
        var product_selling_price=this.refs.product_selling_price.value;
        var product_notes=this.refs.product_notes.value;
        console.log('Send this in a POST request:' + product_name);
    }

    deleteProduct(){

    }

    ProductSearch(){

    }

    render() {
        return (
            <div>
        
            <ol className="breadcrumb pull-right">
                <li><a href="javascript:;">Home</a></li>
                <li className="active">Dashboard</li>
             </ol>
           
           <h1 className="page-header"> Customer List </h1>
           
           <div className="row">
                   <a className="btn btn-success" data-toggle="modal" href='#product-modal'>Add New Product</a>
           </div>
    
           <br/>
    
           <div className="row">
               <div className="col-md-12">
                   <div className="panel panel-inverse">
                       <div className="panel-heading">
                           <div className="panel-heading-btn">
                               <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i className="fa fa-expand"></i></a>
                               <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i className="fa fa-repeat"></i></a>
                               <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i className="fa fa-minus"></i></a>
                               <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i className="fa fa-times"></i></a>
                           </div>
                           <h4 className="panel-title">Data Table - Default</h4>
                       </div>
                       <div className="panel-body">
                           <table id="data-table" className="table table-striped table-bordered">
                               <thead>
                                   <tr>
                                        <th>#</th>
                                        <th>Code</th>
                                        <th>Name</th>
                                        <th>Category</th>
                                        <th>Purchase Price</th>
                                        <th>Selling Price</th>
                                        <th>In Stock </th>
                                        <th>Options</th>
                                   </tr>
                               </thead>
                               <tbody>

                                   {
                                            
                                            this.state.products.map(function(item, i){
                                            
                                                    return  <tr className="odd gradeX" key={i}>
                                                
                                                            <td> {item.id}   </td>
                                                            <td> {item.serial_number} </td>
                                                            <td> {item.name}</td>
                                                            <td> {item.category}</td>
                                                            <td> {item.purchase_price}</td>
                                                            <td> {item.selling_price}</td>
                                                            <td> {item.InStock}</td>
                                                            <td> 
                                                                <button   className="btn btn-info btn-icon btn-circle btn-sm">
                                                                        <i className="fa fa-info"></i>
                                                                </button>
                                                                
                                                                <button  className="btn btn-success btn-icon btn-circle btn-sm">
                                                                    <i className="fa fa-edit"></i>
                                                                </button>
                                    
                                                                <button   className="btn btn-danger btn-icon btn-circle btn-sm">
                                                                    <i className="fa fa-trash"></i>
                                                                </button>
                                                            </td>

                                                    </tr>
                                    
                                            })
                                   }
                                   <tr className="odd gradeX">
                                        <td> </td>
                                        <td> </td>
                                        <td> </td>
                                        <td> </td>
                                        <td> </td>
                                        <td> </td>
                                        <td> </td>
                                        <td> </td>
                                   </tr>
                               </tbody>
                           </table>
                       </div>
                   </div>
               </div>
           </div> 

<div className="modal fade" id="product-modal">
    <div className="modal-dialog">
        <div className="modal-content">
            <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 className="modal-title">Modal title</h4>
            </div>
            <div className="modal-body">
                <form className="form-horizontal form-bordered" onSubmit={this.addProduct} data-parsley-validate="true" encType="multipart/form-data" 	method="post" acceptCharset="utf-8" >

					    <div className="form-group">
					        <label className="control-label col-md-4 col-sm-4">
					            Product Code </label>
					        <div className="col-md-6 col-sm-6">
					            <input className="form-control" type="text" ref="product_code" name="serial_number" required="" placeholder="" defaultValue="433c47cd9d" readOnly=""/>
					        </div>
					    </div>

					    <div className="form-group">
					        <label className="control-label col-md-4 col-sm-4">
					            Product Name </label>
					        <div className="col-md-6 col-sm-6">
					            <input className="form-control" type="text" ref="product_name" name="name" required="" placeholder="Product Name"/>
					        </div>
					    </div>

					    <div className="form-group">
					        <label className="control-label col-md-4 col-sm-4">
					            Category </label>
					        <div className="col-md-6 col-sm-6">
					            <select className="form-control selectpicker" ref="product_category" data-size="10" data-live-search="true" data-style="btn-white" name="category_id">
					                <option value="" selected="">Select Product Category</option>
                                    {
                                        this.state.categories.map(function(item, i){
                                            return <option value="">{item.name}</option>
                                        })
                                    }              
					            </select>
					        </div>
					    </div>

					    <div className="form-group">
					        <label className="control-label col-md-4 col-sm-4">
					            Sub Category </label>
					        <div className="col-md-6 col-sm-6">
					            <select className="form-control selectpicker" ref="product_sub_category" data-size="10" data-live-search="true" data-style="btn-white" name="sub_category_id">
					                <option value="" selected="">Select Product Sub Category</option>
					                <option value="1">Shirt</option>
					            </select>
					        </div>
					    </div>

					    <div className="form-group">
					        <label className="control-label col-md-4 col-sm-4">
					            Purchase Price </label>
					        <div className="col-md-6 col-sm-6">
					            <input className="form-control" type="text" ref="product_purchase_price" name="purchase_price" required="" placeholder="Product Purchase Price"/>
					        </div>
					    </div>

					    <div className="form-group">
					        <label className="control-label col-md-4 col-sm-4">
					            Selling Price </label>
					        <div className="col-md-6 col-sm-6">
					            <input className="form-control" type="text" ref="product_selling_price" name="selling_price" required="" placeholder="Product Selling Price"/>
					        </div>
					    </div>

					    <div className="form-group">
					        <label className="control-label col-md-4 col-sm-4">
					            Notes </label>
					        <div className="col-md-6 col-sm-6">
					            <textarea className="form-control" ref="product_notes" name="note" rows="3"></textarea>
					        </div>
					    </div>

					    <div className="form-group">
					        <label className="control-label col-md-4 col-sm-4">
					            Image </label>
					        <div className="col-md-6 col-sm-6">
					            <input className="form-control"  ref="product_image" type="file" name="userfile" placeholder="Product Image"/>
					        </div>
					    </div>

					    <div className="form-group">
					        <label className="control-label col-md-4 col-sm-4"></label>
					        <div className="col-md-6 col-sm-6">
					            <button type="submit" className="btn btn-success">Save Product</button>
					        </div>
					    </div>


				</form>
				                
            </div>
            <div className="modal-footer">
                <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" className="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
        </div>
        );
    }
}

export default ListProduct;