import React, { Component } from 'react';

class DeleteCustomer extends Component {
    render() {
        return (
            <div className="modal fade" id="customer_delete">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 className="modal-title">Are you sure to delete the information?</h4>
                        </div>
                        <div className="modal-body">
                            <div className="alert alert-danger m-b-0">
                                <h4><i className="fa fa-info-circle"></i> Informations deleted can't be restored !!!</h4>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <a href="javascript:;" className="btn btn-sm btn-white" data-dismiss="modal">Close</a>
                            <a href="" id="delete_link" className="btn btn-sm btn-danger">Delete</a>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default DeleteCustomer;