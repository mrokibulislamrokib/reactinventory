import axios from 'axios';


export function getPendingOrder(){
    var request_url='';
    return axios.get(request_url,{
        
     }).then(function(res){
         if(res.status!==200){

         }else{
             return res.data;
         }
     }).catch(function(error){

     });
}

export function getSingleOrder(){
    var request_url='http://localhost:7000/api/order/';
    return axios.get(request_url,{
       // _id:id
    }).then(function(res){
        if(res.status!==200){
         //   throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
       // throw new Error(error.statusText);
    });
}

export function getOrder(){
    var request_url='http://localhost:7000/api/order/';
    return axios.get(request_url,{
       
    }).then(function(res){
        if(res.status!==200){
        //   throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
        //throw new Error(error.statusText);
    });
}

export function addOrder(order){
    var request_url='http://localhost:7000/api/order/';
    return axios.post(request_url,{
            order_number:order.order_number,
      //      customer_id:order.order_customer,
            order_entries:order.order_entries,
            vat_percentage:order.order_vat_percentage,
            discount_percentage:order.discount_percentage,
            sub_total:order.order_sub_total,
            grand_total:order.order_grand_total,
            due:order.order_due_amount,
            shipping_address:order.order_shipping_address,
            order_status:order.order_status,
            payment_status:order.order_payment_status,
            note:order.order_note,
            creating_timestamp:order.order_creating_timestamp,
            modifying_timestamp:order.modifying_timestamp
    }).then(function(res){
        if(res.status!==200){
          //  throw new Error(res.statusText);
        }else{
            console.log(res.data)
         //   return res.data;
        }
    }).catch(function(error){
        //throw new Error(error.statusText);
    });
}


export function updateOrder(id,name){
    var request_url='http://localhost:7000/api/order/';
    return axios.put(request_url,{
         /*
            _id:id
            order_number:order_number,
            customer_id:customer_id,
            order_entries:order_entries,
            vat_percentage:vat_percentage,
            discount_percentage:discount_percentage,
            sub_total:sub_total,
            grand_total:grand_total,
            due:due,
            shipping_address:shipping_address,
            order_status:order_status,
            payment_status:payment_status,
            note:note,
            creating_timestamp:creating_timestamp,
            modifying_timestamp:modifying_timestamp
        */
    }).then(function(res){
        if(res.status!==200){
            throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
        throw new Error(error.statusText);
    });
}

export function deleteOrder(id){
    var request_url='http://localhost:7000/api/order/';
    return axios.post(request_url,{
        // _id:id
    }).then(function(res){
        if(res.status!==200){
            throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
        throw new Error(error.statusText);
    });
}


const orderslist=
    [
        {id:1,ordercode:"#5665",customer:"John",email:"dvf",date_created:"350",last_modified:"456"},
    ];

export default orderslist;