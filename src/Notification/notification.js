import axios from 'axios';

export function getSingleNotification(){
    var request_url='';
    return axios.get(request_url,{
       
    }).then(function(res){
        if(res.status!==200){
            throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
        throw new Error(error.statusText);
    });
}

export function getNotification(){
    var request_url='';
    return axios.get(request_url,{
       
    }).then(function(res){
        if(res.status!==200){
            throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
        throw new Error(error.statusText);
    });
}

export function addNotification(name){
    var request_url='';
    return axios.post(request_url,{
        name:name
    }).then(function(res){
        if(res.status!==200){
            throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
        throw new Error(error.statusText);
    });
}


export function updateNotification(id,name){
    var request_url='';
    return axios.put(request_url,{
        id:id,
        name:name
    }).then(function(res){
        if(res.status!==200){
            throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
        throw new Error(error.statusText);
    });
}

export function deleteNotification(id){
    var request_url='';
    return axios.post(request_url,{
        id:id
    }).then(function(res){
        if(res.status!==200){
            throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
        throw new Error(error.statusText);
    });
}



const Notificationslist=
    [
        {id:1,Company:"RAHIM AFRUZ",Name:'John',Email:'Doe',Phone:'0967'},
    ];

export default Notificationslist;