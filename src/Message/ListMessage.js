import React, { Component } from 'react';
import Messageslist,{getMessage,addMessage,updateMessage,deleteMessage} from './message';

class ListMessage extends Component {

    constructor(props) {
        super(props);
        this.addMessage=this.addMessage.bind(this);
        this.updateMessage=this.updateMessage.bind(this);
    }

    addMessage(e){

    }

    updateMessage(e){

    }

    render() {
        return (
            <div>
                <div className="row">
                    <div className="col-md-4 col-sm-4">
                        <a href="" className="btn btn-success p-l-40 p-r-40 btn-sm">Compose New Message </a>
                        <br/><br/>
                        <ul className="nav nav-pills nav-stacked nav-sm"></ul>
                    </div>
	                
                    <div className="col-md-8 col-sm-8 ui-sortable">
		                    <blockquote> <p>Please Select A Message To Read</p></blockquote>
		                    <img src="assets/message.jpg"/> 

                            <div className="panel panel-inverse">
                                        
                                <div className="panel-heading">
                                    <div className="panel-heading-btn">
                                        <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i className="fa fa-expand"></i></a>
                                        <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse" data-original-title="" title=""><i className="fa fa-minus"></i></a>
                                    </div>
                                    <h4 className="panel-title">Compose A Message</h4>
                                </div>

                                <div className="panel-body">
                	
                                    <form onSubmit={this.addMessage} className="form-horizontal" data-parsley-validate="true" encType="multipart/form-data" method="post" acceptCharset="utf-8" noValidate="">

                                        <div className="form-group">
                                            <div className="col-md-12 col-sm-12">

                                                    <select className="form-control selectpicker" data-size="10" data-live-search="true" data-style="btn-white" name="receiver" required="" data-parsley-id="1488">
                                                        <option defaultValue="" select="">Select User</option>
                                                        <optgroup label="Customer">
                                                            <option value="customer-1">
                                                                Walk In Clients </option>
                                                        </optgroup>
                                                        <optgroup label="Sales Staff">
                                                            <option value="employee-1">
                                                                test </option>
                                                        </optgroup>
                                                        <optgroup label="Purchase Staff">
                                                            <option value="employee-2">
                                                                test2 </option>
                                                        </optgroup>
                                                    </select>
                                                            
                                            </div>
                                        </div>

                                        <div className="form-group">
                                            <div className="col-md-12 col-sm-12">
                                                    <textarea className="textarea form-control" id="wysihtml5" ref="message_body" name="message_body" rows="15" required=""></textarea>
                                            </div>
                                        </div>


                                        <div className="form-group">
                                            <div className="col-md-6 col-sm-6">
                                                <button type="submit" className="btn btn-success">Send</button>
                                            </div>
                                        </div>

                                    </form> 

                                </div>
                            </div>
                        </div>
                    </div>

            </div>
        );
    }
}

export default ListMessage;