import axios from 'axios';

export function getSubCategoryByCategory(){
    var request_url='http://localhost:7000/api/subcategory/';
    return axios.get(request_url,{
       
    }).then(function(res){
        if(res.status!==200){
            throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
        throw new Error(error.statusText);
    });

}

export function getSingleSubCategory(){
    var request_url='http://localhost:7000/api/subcategory/';
    return axios.get(request_url,{
       //_id:id
    }).then(function(res){
        if(res.status!==200){
            throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
        throw new Error(error.statusText);
    });
}

export function getSubCategory(){
    var request_url='http://localhost:7000/api/subcategory/';
    return axios.get(request_url,{
       
    }).then(function(res){
        if(res.status!==200){
            throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
        throw new Error(error.statusText);
    });
}

export function addSubCategory(sub){
    var request_url='http://localhost:7000/api/subcategory/';
    return axios.post(request_url,{
        name:sub.sub_category_name,
        description:sub.sub_category_description
       // category_id:category_id 
    }).then(function(res){
        if(res.status!==200){
    //        throw new Error(res.statusText);
        }else{
            console.log(res.data);
            return res.data;
        }
    }).catch(function(error){
        console.log(error);
    //    throw new Error(error.statusText);
    });
}


export function updateSubCategory(id,name){
    var request_url='http://localhost:7000/api/subcategory/';
    return axios.put(request_url,{
       /*
        _id:id,
        name:name,
        description:description,
        category_id:category_id
        */
    }).then(function(res){
        if(res.status!==200){
            throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
        throw new Error(error.statusText);
    });
}

export function deleteSubCategory(id){
    var request_url='http://localhost:7000/api/subcategory/';
    return axios.post(request_url,{
        //_id:id
    }).then(function(res){
        if(res.status!==200){
            throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
        throw new Error(error.statusText);
    });
}


const subcategorieslist=
    [
        {id:1,Name:"John",category:'com',description:"dvf",},
    ];

export default subcategorieslist;