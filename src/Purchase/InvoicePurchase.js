import React, { Component } from 'react';
import {getSetting} from '../Setting/set';
import {getPurchase} from '../Purchase/purchase';
import {getSale} from '../Sale/sale';
import {getProduct} from '../Product/product';
import {getCustomer} from '../Customer/customer';
import {getSupplier} from '../Supplier/supplier';
class InvoicePurchase extends Component {

    constructor(props){
        super(props);
        this.state={}
    }
    
    componentDidMount() {
        
    }
    
    

    render() {
        return (
<div>
    <div className="invoice">
        <div className="invoice-company">
            <span className="pull-right hidden-print">
                <a href="javascript:;" onclick="window.print()" className="btn btn-sm btn-success m-b-10"><i className="fa fa-print m-r-5"></i> Print</a>
            </span>
            Ezzitech        
        </div>
            
        <div className="invoice-header">
            <div className="invoice-from">
                <small>From</small>
                <address className="m-t-5 m-b-5">
                    <strong>Ezzitech</strong><br/>
                    Bangladesh<br/>
                    Phone: 01859343274<br/>
                    Email: info@ezzitech.com                
                </address>
            </div>
            <div className="invoice-to">
                <small>To</small>
                <address className="m-t-5 m-b-5">
                    <strong>Baby Cloth Provider</strong><br/>
                    Test<br/>
                    Phone: 626262<br/>
                    Email: test@email.com                </address>
            </div>
            <div className="invoice-date">
                <div className="date m-t-5">15th Nov, 2017</div>
                <div className="invoice-detail">
                    <strong>Purchase Code: 3d761faf13</strong>
                </div>
            </div>
        </div>
            
        <div className="invoice-content">
            <div className="table-responsive">
                <table className="table table-invoice table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Serial No</th>
                            <th>Products</th>
                            <th>Unit Price</th>
                            <th>Quantity</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                                            <tr>
                            <td>1</td>
                            <td>b912b9777c</td>
                            <td>Shirt-123</td>
                            <td>TK 500</td>
                            <td>10</td>
                            <td>TK 5000</td>
                        </tr>
                                        
                    </tbody>
                </table>
            </div>
            <div className="invoice-price">
                <div className="invoice-price-left">
                </div>
                <div className="invoice-price-right">
                    <small>Total Amount</small>
                    TK 5000                </div>
            </div>
        </div>
        <div className="invoice-footer text-muted">
            <p className="text-center m-b-5">
                Thank You For Your Business            
            </p>
            <p className="text-center">
                <span className="m-r-10"><i className="fa fa-globe"></i> Ezzitech</span>
                <span className="m-r-10"><i className="fa fa-phone"></i> 01859343274</span>
                <span className="m-r-10"><i className="fa fa-envelope"></i> info@ezzitech.com</span>
            </p>
        </div>
    </div>
</div>
        );
    }
}

export default InvoicePurchase;