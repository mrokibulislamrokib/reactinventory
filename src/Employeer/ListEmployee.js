import React, { Component } from 'react';
import employeeslist,{getEmployee,addEmployee,updateEmployee,deleteEmployee} from './employee';
//import employee from './employee';
class  ListEmployee extends Component {

    constructor(props) {
        super(props);
        this.state = {
            employee_name:'',
            employee_email:'',
            employee_password:'',
            employee_type:'',
            employee_phone:'',
            employee_address:'',
            employee_image:'',
            employees: []
        };
        this.addEmployee=this.addEmployee.bind(this);
        this.updateEmployee=this.updateEmployee.bind(this);
        this.deleteEmployee=this.deleteEmployee.bind(this);
        this.employeeSearch=this.employeeSearch.bind(this);

    }

    componentDidMount(){
        this.state.employees=employeeslist;
       /*  
       getEmployee().then(response => {
            this.setState({
                employees: response
            }); 
            console.log(response);
        }); */

        console.log(getEmployee());
    }

    componentWillReceiveProps(){

    }
    
    addEmployee(e){
        e.preventDefault();
        var employee_name=this.refs.employee_name.value;
        var employee_email=this.refs.employee_email.value;
        var employee_password=this.refs.employee_password.value;
        var employee_type=this.refs.employee_password.value;
        var employee_phone=this.refs.employee_phone.value;
        var employee_address=this.refs.employee_address.value;

        var employee={
            employee_name:employee_name,
            employee_email:employee_email,
            employee_password:employee_password,
            employee_type:employee_type,
            employee_phone:employee_phone,
            employee_address:employee_address
        }

        addEmployee(employee);

      //  this.state.employees.push(employee);

        console.log('Send this in a POST request:' + employee_name);
    }

    updateEmployee(e){
        var employee_name=this.refs.employee_name.value;
        var employee_email=this.refs.employee_email.value;
        var employee_password=this.refs.employee_password.value;
        var employee_type=this.refs.employee_password.value;
        var employee_phone=this.refs.employee_phone.value;
        var employee_address=this.refs.employee_address.value;
        console.log('Send this in a POST request:' + employee_name);
    }

    deleteEmployee(e){

    }

    employeeSearch(e){

    }

    render() {
     //   console.log(this.state.employees);
        return (
        
        <div>
            <ol className="breadcrumb pull-right">
                <li><a href="javascript:;">Home</a></li>
                <li className="active">Dashboard</li>
            </ol>

            <h1 className="page-header">Dashboard <small>header small text goes here...</small></h1>
            
            <div className="row">
                    <a className="btn btn-success" data-toggle="modal" href='#employeer-modal'>Add New employeer</a>
            </div>

            <br/>

			<div className="row">
			    <div className="col-md-12">
                    <div className="panel panel-inverse">
                        <div className="panel-heading">
                            <div className="panel-heading-btn">
                                <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i className="fa fa-expand"></i></a>
                                <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i className="fa fa-repeat"></i></a>
                                <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i className="fa fa-minus"></i></a>
                                <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i className="fa fa-times"></i></a>
                            </div>
                            <h4 className="panel-title">Data Table - Default</h4>
                        </div>
                        <div className="panel-body">
                            <table id="data-table" className="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Address</th>
                                        <th>Phone</th>
                                        <th>Type </th>
                                        <th>Options</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    {

                                        this.state.employees.map(function(item, i){
                                            return  <tr className="odd gradeX" key={i}>
                                                    
                                                            <td> {item.id} </td>
                                                            <td> {item.Name}</td>
                                                            <td> {item.Email}</td>
                                                            <td> {item.address}</td>
                                                            <td> {item.Phone}</td>
                                                            <td> {item.Type}</td>
                                                            <td> 
                                                            <button   className="btn btn-info btn-icon btn-circle btn-sm">
                                                <i className="fa fa-info"></i>
                                        </button>
                                        <button  className="btn btn-success btn-icon btn-circle btn-sm">
                                                <i className="fa fa-edit"></i>
                                        </button>
                                        <button   className="btn btn-danger btn-icon btn-circle btn-sm">
                                                <i className="fa fa-trash"></i>
                                        </button>
                                                            </td>

                                                    </tr>
                                        
                                        })
                                    }
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div className="modal fade" id="employeer-modal">
    <div className="modal-dialog">
        <div className="modal-content">
            <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 className="modal-title">Modal title</h4>
            </div>
            <div className="modal-body">
                <form onSubmit={this.addEmployee} className="form-horizontal form-bordered" encType="application/x-www-form-urlencoded"   method="post" acceptCharset="utf-8">
                    <div className="form-group">
                        <label className="control-label col-md-4 col-sm-4">
                            Employee Name </label>
                        <div className="col-md-6 col-sm-6">
                            <input className="form-control" type="text" ref="employee_name" name="name" required="" placeholder="Employee Name"/>
                        </div>
                    </div>

                    <div className="form-group">
                        <label className="control-label col-md-4 col-sm-4">
                            Email </label>
                        <div className="col-md-6 col-sm-6">
                            <input className="form-control" type="email" ref="employee_email" name="email" required="" placeholder="Employee Email"/>
                        </div>
                    </div>

                    <div className="form-group">
                        <label className="control-label col-md-4 col-sm-4">
                            Password </label>
                        <div className="col-md-6 col-sm-6">
                            <input className="form-control" type="password" ref="employee_password" name="password" required="" placeholder="Employee Password"/>
                        </div>
                    </div>

                    <div className="form-group">
                        <label className="control-label col-md-4 col-sm-4">
                            Type </label>
                        <div className="col-md-6 col-sm-6">
                            <select className="form-control selectpicker" ref="employee_type" data-size="10" data-live-search="true" data-style="btn-white" name="type" required="">
                                <option value="" selected="">Select Employee Type</option>
                                <option value="1">Sales Staff</option>
                                <option value="2">Purchase Staff</option>
                            </select>
                        </div>
                    </div>

                    <div className="form-group">
                        <label className="control-label col-md-4 col-sm-4">
                            Phone </label>
                        <div className="col-md-6 col-sm-6">
                            <input className="form-control" type="text" ref="employee_phone" name="phone" placeholder="Employee Phone Number"/>
                        </div>
                    </div>

                    <div className="form-group">
                        <label className="control-label col-md-4 col-sm-4">
                            Address </label>
                        <div className="col-md-6 col-sm-6">
                            <textarea className="form-control" name="address" ref="employee_address" placeholder="Employee Address" rows="3"></textarea>
                        </div>
                    </div>

                    <div className="form-group">
                        <label className="control-label col-md-4 col-sm-4">
                            Image </label>
                        <div className="col-md-6 col-sm-6">
                            <input className="form-control" type="file" name="userfile" ref="employee_image" placeholder="Employee Image"/>
                        </div>
                    </div>

                    <div className="form-group">
                        <label className="control-label col-md-4 col-sm-4"></label>
                        <div className="col-md-6 col-sm-6">
                            <button type="submit" className="btn btn-success">Save Employee</button>
                        </div>
                    </div>

				</form>
				                
            </div>
            <div className="modal-footer">
                <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" className="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
        </div>
        );
    }
}

export default ListEmployee;