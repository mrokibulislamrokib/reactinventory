import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import {getCustomer,getTotalCustomer} from '../Customer/customer';
import {getOrder} from '../Order/order';
import {getSale} from '../Sale/sale';
import {getPurchase} from '../Purchase/purchase';

class Dashboard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            totalCustomer:''
        };
    }

    componentDidMount(){

        getTotalCustomer().then(response => {
            this.setState({
                totalCustomer: response
            });
        });
/*
        getProduct().then(response => {
            this.setState({
                products: response
            });
        });

        getSale().then(response => {
            this.setState({
                invoices: response
            });
        });
        var Order=getOrder();
        var Sale=getSale(); */

    //   console.log(Customer);
    }

    componentWillReceiveProps(){

    }


    render() {
        //console.log(this.state.totalCustomer);
        return (
            <div>
                <ol className="breadcrumb pull-right">
                    <li><a href="">Dashboard</a></li>
                </ol>
                <h1 className="page-header">Dashboard</h1>

                <div className="row">
                    <div className="col-md-3 col-sm-6">
                        <div className="widget widget-stats bg-green">
                            <div className="stats-icon"><i className="fa fa-users"></i></div>
                            <div className="stats-info">
                                <h4>Total Customers</h4>
                                <p>1</p>	
                            </div>
                            <div className="stats-link">
                                <Link to="/customer">View Customer List <i className="fa fa-arrow-circle-o-right"></i></Link>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-3 col-sm-6">
                        <div className="widget widget-stats bg-blue">
                            <div className="stats-icon"><i className="fa fa-bell"></i></div>
                            <div className="stats-info">
                                <h4>Pending Orders</h4>
                                <p>0</p>	
                            </div>
                            <div className="stats-link">
                            <Link to="/order">View All Orders <i className="fa fa-arrow-circle-o-right"></i></Link>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-3 col-sm-6">
                        <div className="widget widget-stats bg-purple">
                            <div className="stats-icon"><i className="fa fa-dollar"></i></div>
                            <div className="stats-info">
                                <h4>Total Income Amount</h4>
                                <p>3822.88</p>	
                            </div>
                            <div className="stats-link">
                                <Link to="/sale_invoice">View Sale Invoices <i className="fa fa-arrow-circle-o-right"></i></Link>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-3 col-sm-6">
                        <div className="widget widget-stats bg-black">
                            <div className="stats-icon"><i className="fa fa-money"></i></div>
                            <div className="stats-info">
                                <h4>Total Purchase Amount</h4>
                                <p>5000</p>	
                            </div>
                            <div className="stats-link">
                                <Link to="/purchase_history">View Purchase History <i className="fa fa-arrow-circle-o-right"></i></Link>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

export default Dashboard;