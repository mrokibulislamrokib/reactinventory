
import axios from 'axios';

export function getSingleCategory(){
    var request_url='http://localhost:7000/api/category/';
    return axios.get(request_url,{
       // _id:id
    }).then(function(res){
        if(res.status!==200){
            throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
        throw new Error(error.statusText);
    });
}

export function getCategory(){
    var request_url='http://localhost:7000/api/category/';
    return axios.get(request_url,{
       
    }).then(function(res){
        if(res.status!==200){
         //   throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
      //  throw new Error(error.statusText);
    });
}

export function addCategory(category){
    var request_url='http://localhost:7000/api/category/';
    return axios.post(request_url,{
        name:category.name,
        description:category.description 
    }).then(function(res){
        if(res.status!==200){
          //  throw new Error(res.statusText);
        }else{
            console.log(res.data)
          //  return res.data;
        }
    }).catch(function(error){
       // throw new Error(error.statusText);
    });
}


export function updateCategory(id,name){
    var request_url='http://localhost:7000/api/category/';
    return axios.put(request_url,{
        /*
        _id:id,
        name:name,
        description:description 
        */
    }).then(function(res){
        if(res.status!==200){
            throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
        throw new Error(error.statusText);
    });
}

export function deleteCategory(id){
    var request_url='http://localhost:7000/api/category/';
    return axios.post(request_url,{
        // _id:id
    }).then(function(res){
        if(res.status!==200){
            throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
        throw new Error(error.statusText);
    });
}


const categorieslist=
    [
        {id:1,Name:"John",description:"dvf",},
    ];

export default categorieslist;