import React, { Component } from 'react';
import { HashRouter,Switch,Route,Link} from 'react-router-dom';

import EditSupplier from './Supplier/EditSupplier';
import DeleteSupplier from './Supplier/DeleteSupplier';
import Dashboard       from './Dashboard//Dashboard';
import ListCustomer    from './Customer/Listcustomer';
import ListEmployeer   from './Employeer/ListEmployee';
import ListSupplier    from './Supplier/ListSupplier';
import ListOrder       from './Order/ListOrder';
import ListProduct     from './Product/ListProduct';
import HistoryPurchase from './Purchase/HistoryPurchase';
import InvoiceSale     from './Sale/InvoiceSale';
import ListTax         from './Tax/ListTax';
import Setting         from './Setting/Setting';
import ProfileSetting  from './Profile/ProfileSetting';
import Login           from './Profile/Login';
import ListMessage     from './Message/ListMessage';
import ListProductCategory      from './Product/ListProductCategory';
import ListProductBarcode       from './Product/ListProductBarcode';
import ListProductDamaged       from './Product/ListProductDamaged';
import ListProductSubCategory from './Product/ListProductSubCategory';
import ListNotification from './Notification/ListNotification';

import ListPayment              from './Report/Listpayment';
import ListCustomerPayment      from './Report/ListCustomerPayment';


import CreateOrder              from './Order/CreateOrder';
import CreateSale               from './Sale/CreateSale';
import CreatePurchase           from './Purchase/CreatePurchase';

import InvoiceView      from     './Sale/InvoiceView';
import InvoicePurchase  from     './Purchase/InvoicePurchase';
//<Route path='/purchase_add' component={CreatePurchase}/>
class Main extends Component {
    render() {
        return(
            <main>
                <Switch>
                    <Route exact path='/' component={Dashboard}/>
                    <Route path='/customer' component={ListCustomer}/>
                    <Route path='/employee' component={ListEmployeer}/>
                    <Route path='/supplier' component={ListSupplier}/>
                    <Route path='/product' component={ListProduct}/>
                    <Route path='/product_barcode' component={ListProductBarcode}/>
                    <Route path='/product_category' component={ListProductCategory}/>
                    <Route path='/product_sub_category' component={ListProductSubCategory}/>
                    <Route path='/damaged_product' component={ListProductDamaged}/>
                    <Route path='/order' component={ListOrder}/>
                    <Route path='/order_add' component={CreateOrder}/>
                    <Route path='/purchase_add' component={CreatePurchase}/>
                    <Route path='/purchase_history' component={HistoryPurchase}/>
                    <Route path='/sale_add' component={CreateSale}/>
                    <Route path='/sale_invoice' component={InvoiceSale}/>
                    <Route path='/payment' component={ListPayment}/>
                    <Route path='/customer_payment' component={ListCustomerPayment}/>
                    <Route path='/messaging' component={ListMessage}/>
                    <Route path='/tax_setting' component={ListTax}/>
                    <Route path='/setting' component={Setting}/>
                    <Route path='/profile' component={ProfileSetting}/>
                    <Route path='/notification' component={ListNotification}/>
                    <Route path='/login' component={Login}/>
                    <Route path='/invoiceView' component={InvoiceView}/>
                    <Route path='/invoicePurchase' component={InvoicePurchase}/>
                    <Route exact={true} path="/supplieredit/:id" component={EditSupplier}/>
		            <Route exact={true} path="/supplierdelete/:id" component={DeleteSupplier}/>
                    
                </Switch>
            </main>
        );
    }
}

export default Main;