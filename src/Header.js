import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import Dashboard from './Dashboard//Dashboard';
import ListCustomer    from './Customer/Listcustomer';
import ListEmployeer   from './Employeer/ListEmployee';
import ListSupplier    from './Supplier/ListSupplier';
class Header extends Component {
    render() {
        return (
            <div>
                <div id="header" className="header navbar navbar-default navbar-fixed-top">
                    <div className="container-fluid">
                        <div className="navbar-header">
                            <Link to="/" className="navbar-brand" > <span className="navbar-logo"></span>  Inventory</Link>
                            <button type="button" className="navbar-toggle" data-click="sidebar-toggled">
                                <span className="icon-bar"></span>
                                <span className="icon-bar"></span>
                                <span className="icon-bar"></span>
                            </button>
                        </div>
                        <ul className="nav navbar-nav navbar-right">
                            <li>
                                <form className="navbar-form full-width">
                                    <div className="form-group">
                                        <input type="text" className="form-control" placeholder="Enter keyword" />
                                        <button type="submit" className="btn btn-search"><i className="fa fa-search"></i></button>
                                    </div>
                                </form>
                            </li>
                            <li className="dropdown">
                                <a href="javascript:;" data-toggle="dropdown" className="dropdown-toggle f-s-14">
                                    <i className="fa fa-bell-o"></i>
                                    <span className="label">5</span>
                                </a>
                                <ul className="dropdown-menu media-list pull-right animated fadeInDown">
                                    <li className="dropdown-header">Notifications (5)</li>
                                    <li className="media">
                                        <a href="javascript:;">
                                            <div className="media-left"><i className="fa fa-bug media-object bg-red"></i></div>
                                            <div className="media-body">
                                                <h6 className="media-heading">Server Error Reports</h6>
                                                <div className="text-muted f-s-11">3 minutes ago</div>
                                            </div>
                                        </a>
                                    </li>
                                    <li className="dropdown-footer text-center">
                                        <Link to="/notification">View more</Link>
                                    </li>
                                </ul>
                            </li>


                            <li className="dropdown navbar-user">
                                <a href="javascript:;" className="dropdown-toggle" data-toggle="dropdown">
                                    <img src="assets/img/user-13.jpg" alt="" /> 
                                    <span className="hidden-xs">Adam Schwartz</span> <b className="caret"></b>
                                </a>
                                <ul className="dropdown-menu animated fadeInLeft">
                                    <li className="arrow"></li>
                                    <li> <Link to="/profile">Profile</Link></li>
                                    <li> <Link to="/messaging">Messaging</Link></li>
                                    <li> <Link to="/setting">System Setting</Link></li>
                                    <li><a href="">Log Out</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>

                <div id="sidebar" className="sidebar">
                    <div data-scrollbar="true" data-height="100%">

                        <ul className="nav">
                            <li className="nav-profile">
                                <div className="image"> <a href="javascript:;"><img src="assets/img/user-13.jpg" alt="" /></a></div>
                                <div className="info">Sean Ngu<small>Front end developer</small></div>
                            </li>
                        </ul>


                        <ul className="nav">
                            <li className="active"><Link to='/'><i className="fa fa-users"></i> <span>Dashboard </span></Link></li>
                            <li><Link to='/customer'><i className="fa fa-users"></i>customer</Link></li>
                            <li className=""> <Link    to="/supplier"> <i className="fa fa-users"></i> <span>Supplier</span> </Link></li>
                            <li className=""> <Link    to="/employee"> <i className="fa fa-users"></i> <span>Employeer</span> </Link></li>
                            <li className="has-sub">
                                <a href="javascript:;"><b className="caret pull-right"></b><i className="fa fa-shopping-cart"></i>
                                    <span>Manage Products</span>
                                </a>
                                <ul className="sub-menu">
                                    <li className=""> <Link  to="/product">Product</Link> </li>
                                    <li className=""> <Link  to="/product_barcode">Prodcut Barcode</Link> </li>
                                    <li className=""> <Link  to="/product_category">Category</Link></li>
                                    <li className=""> <Link  to="/product_sub_category">Sub Category</Link> </li>
                                    <li className="="> <Link to="/damaged_product">Damaged Product</Link></li>
                                </ul>
                            </li>

                            <li className="has-sub">
                                <a href="javascript:;"><b className="caret pull-right"></b><i className="fa fa-bell"></i>
                                    <span>Manage Orders</span>
                                </a>
                                <ul className="sub-menu">
                                    <li className=""> <Link to="/order_add">Create New Order</Link></li>
                                    <li className=""> <Link to="/order">All Orders</Link></li>
                                </ul>
                            </li>

                            <li className="has-sub">
                                <a href="javascript:;">
                                    <b className="caret pull-right"></b>
                                    <i className="fa fa-bell"></i>
                                    <span>Manage Purchases</span>
                                </a>
                                <ul className="sub-menu">
                                    <li className=""> <Link to="/purchase_add">New Purchases</Link></li>
                                    <li className=""> <Link to="/purchase_history"> Purchase History</Link></li>
                                </ul>
                            </li>

                            <li className="has-sub">
                                <a href="javascript:;">
                                    <b className="caret pull-right"></b>
                                    <i className="fa fa-bell"></i>
                                    <span>Manage Sales</span>
                                </a>
                                <ul className="sub-menu">
                                    <li className=""> <Link to="/sale_add">New Sales</Link></li>
                                    <li className=""> <Link to="/sale_invoice"> Sales Invoice</Link></li>
                                </ul>
                            </li>


                            <li className="has-sub">
                                <a href="javascript:;">
                                    <b className="caret pull-right"></b>
                                    <i className="fa fa-bell"></i>
                                    <span>Payments</span>
                                </a>
                                <ul className="sub-menu">
                                    <li className=""> <Link to="/payment">Payments</Link></li>
                                    <li className=""> <Link to="/customer_payment">Customer Payments</Link></li>
                                </ul>
                            </li>

                            <li className=""> <Link to="/messaging"> <i className="fa fa-users"></i> <span>Messaging</span> </Link></li>

                            <li className=""> <Link to="/tax_setting"> <i className="fa fa-users"></i> <span>Tax Settings</span> </Link></li>

                            <li className=""> <Link to="/setting"> <i className="fa fa-users"></i> <span>System Setting</span> </Link></li>
                            <li><a href="javascript:;" className="sidebar-minify-btn" data-click="sidebar-minify"><i className="fa fa-angle-double-left"></i></a></li>

                        </ul>
                    </div>
                </div>
                <div className="sidebar-bg"></div>
            </div>
        );
    }
}

export default Header;