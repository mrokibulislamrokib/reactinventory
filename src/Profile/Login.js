import React, { Component } from 'react';

import {getUser} from './user';

class Login extends Component {
    
    constructor(props){
        super(props);
        this.state={
            login_email:'',
            login_password:'',
            login_remember:''
        }
        this.signin=this.signin.bind(this);
    }

    signin(e){
        e.preventDefault();
        var login_email=this.refs.login_email.value;
        var login_password=this.refs.login_password.value;
    }
    
    render() {
        return (      
        <div>
            <div className="login-cover">
                <div className="login-cover-image"><img src="assets/img/login-bg/bg-1.jpg" data-id="login-cover-image" alt="" /></div>
                <div className="login-cover-bg"></div>
            </div>

            <div id="page-container" className="fade in">
                <div className="login login-v2">
                    <div className="login-header">
                        <div className="brand">
                            <span className="logo"></span> Color Admin
                            <small>responsive bootstrap 3 admin template</small>
                        </div>
                        <div className="icon">
                            <i className="fa fa-sign-in"></i>
                        </div>
                    </div>
                    <div className="login-content">
                        <form onSubmit={this.signin} method="POST" className="margin-bottom-0">
                            <div className="form-group m-b-20">
                                <input type="text" className="form-control input-lg" ref="login_email" placeholder="Email Address" required />
                            </div>
                            <div className="form-group m-b-20">
                                <input type="password" className="form-control input-lg" ref="login_password" placeholder="Password" required />
                            </div>
                            <div className="checkbox m-b-20">
                                <label>
                                    <input type="checkbox" ref="login_remember" /> Remember Me
                                </label>
                            </div>
                            <div className="login-buttons">
                                <button type="submit" className="btn btn-success btn-block btn-lg">Sign me in</button>
                            </div>
                            <div className="m-t-20">
                                Not a member yet? Click <a href="#">here</a> to register.
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        );
    }
}

export default Login;