import axios from 'axios';


export function getSingleProduct(){
    var request_url='http://localhost:7000/api/product/';
    return axios.get(request_url,{
       // _id:id
    }).then(function(res){
        if(res.status!==200){
            throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
        throw new Error(error.statusText);
    });
}

export function getProductByCategory(){
    var request_url='http://localhost:7000/api/product/';
    return axios.get(request_url,{
       
    }).then(function(res){
        if(res.status!==200){
            throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
        throw new Error(error.statusText);
    });
}

export function getProduct(){
    var request_url='http://localhost:7000/api/product/';
    return axios.get(request_url,{

    }).then(function(res){
        if(res.status!==200){
           // throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
      //  throw new Error(error.statusText);
    });
}

export function addProduct(product){
    var request_url='http://localhost:7000/api/product/';
    return axios.post(request_url,{
        serial_number:product.product_code,
        // category_id:product.product_category,
       //  sub_category_id:product.product_sub_category,
         name:product.product_name,
         purchase_price:product.product_purchase_price,
         selling_price:product.product_selling_price,
         note:product.product_notes,
    }).then(function(res){
        if(res.status!==200){
        //    throw new Error(res.statusText);
        }else{
            console.log(res.data);
           // return res.data;
        }
    }).catch(function(error){
       // throw new Error(error.statusText);
    });
}


export function updateProduct(id,name){
    var request_url='http://localhost:7000/api/product/';
    return axios.put(request_url,{
          /* 
       _id:id
       serial_number:serial_number,
       category_id:category_id,
       sub_category_id:sub_category_id,
       name:name,
       purchase_price:purchase_price,
       selling_price:selling_price,
       note:note,
       stock_quantity:stock_quantity */
    }).then(function(res){
        if(res.status!==200){
            throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
        throw new Error(error.statusText);
    });
}

export function deleteProduct(id){
    var request_url='http://localhost:7000/api/product/';
    return axios.post(request_url,{
        //_id:id
    }).then(function(res){
        if(res.status!==200){
            throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
        throw new Error(error.statusText);
    });
}

const productslist=
    [
        {id:1,code:"#5665",Name:"John",Category:"dvf",PurchasePrice:"350",SellingPrice:"456",InStock:"20"},
    ];

export default productslist;