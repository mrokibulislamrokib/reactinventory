import React, { Component } from 'react';
import Saleslist ,{getSale,addSale,updateSale,deleteSale} from './sale';
class InvoiceSale extends Component {
    
    constructor(props) {
        super(props);
        this.state={
            sales:Saleslist
        };
    }

    componentDidMount(){
        getSale().then(response => {
            this.setState({
                sales: response
            });
        });
    }

    componentWillReceiveProps(){

    }

    render() {
        console.log(this.state.sales)
       return (
    
            <div>
                
                <ol className ="breadcrumb pull-right">
                    <li><a href="javascript:;">Home</a></li>
                    <li className ="active">Dashboard</li>
                </ol>
            
            <h1 className ="page-header"> Sale List </h1>
            
            <div className ="row">
                    <a className ="btn btn-success" data-toggle="modal" href='#order-modal'>Add New Sale</a>
            </div>

            <br/>

            <div className ="row">
                <div className ="col-md-12">
                    <div className ="panel panel-inverse">
                        <div className ="panel-heading">
                            <div className ="panel-heading-btn">
                                <a href="javascript:;" className ="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i className ="fa fa-expand"></i></a>
                                <a href="javascript:;" className ="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i className ="fa fa-repeat"></i></a>
                                <a href="javascript:;" className ="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i className ="fa fa-minus"></i></a>
                                <a href="javascript:;" className ="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i className ="fa fa-times"></i></a>
                            </div>
                            <h4 className ="panel-title">Data Table - Default</h4>
                        </div>
                        <div className ="panel-body">
                            <table id="data-table" className ="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                            <th>#</th>
                                            <th>Code</th>
                                            <th>Customer</th>
                                            <th>email</th>
                                            <th>Options </th>
                                    </tr>
                                </thead>
                                <tbody>
                                {
                                       this.state.sales.map(function(item,i){

                                     return  <tr className="odd gradeX" key={i}>
                                                <td> {item.id}       </td>
                                                <td> {item.invoice_code} </td>
                                                <td> {item.customer}  </td>
                                                <td> {item.email} </td>
                                                <td> </td>
                                        </tr>

                                       })
                                   }
                                  
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
       );
    }
}

export default InvoiceSale;