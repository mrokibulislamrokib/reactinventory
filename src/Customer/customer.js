import axios from 'axios';

export function getSingleCustomer(id){
    var request_url='http://localhost:7000/api/customer/';
    return axios.get(request_url,{
         _id:id
    }).then(function(res){
        if(res.status!==200){
         //   throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
      //  throw new Error(error.statusText);
    });
}

export function getTotalCustomer(){
    var request_url='http://localhost:7000/api/customer/';
    return axios.get(request_url,{
       
    }).then(function(res){
        if(res.status!==200){
        //    throw new Error(res.statusText);
        }else{
            return Object.keys(res.data).length;
        }
    }).catch(function(error){
       // throw new Error(error.statusText);
    });
}


export function getCustomer(){
    var request_url='http://localhost:7000/api/customer/';
    return axios.get(request_url,{}).then(function(res){
        if(res.status!==200){ 
            //throw new Error(res.statusText);
        }else{
          //  console.log(res.data);
          //  console.log(Object.keys(res.data).length);
            return res.data;
        }
    }).catch(function(error){
       // throw new Error(error.statusText);
    });
    
}

export function addCustomer(customer){
    var request_url='http://localhost:7000/api/customer/';
    return axios.post(request_url,{
            customer_code:customer.customer_code,
            customer_name:customer.customer_name,
            email:customer.customer_email,
            password:customer.customer_password,
            gender:customer.customer_gender,
            address:customer.customer_address,
            phone:customer.customer_phone,
            discount_percentage:customer.customer_discount,
    }).then(function(res){
        if(res.status!==200){
           // throw new Error(res.statusText);
        }else{
            console.log(res.data);
           // return res.data;
        }
    }).catch(function(error){
        //throw new Error(error.statusText);
    });
}


export function updateCustomer(customer){
    var request_url='http://localhost:7000/api/customer/';
    return axios.put(request_url,{
        /*
            _id:id
            customer_code:customer_code,
            customer_name:customer_name,
            email:email,
            password:password,
            gender:gender,
            address:address,
            phone:phone,
            discount_percentage:discount_percentage,
            image:image
        */
    }).then(function(res){
        if(res.status!==200){
        //    throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
       // throw new Error(error.statusText);
    });
}

export function deleteCustomer(id){
    var request_url='http://localhost:7000/api/customer/';
    return axios.post(request_url,{
        // _id:id
    }).then(function(res){
        if(res.status!==200){
            throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
        throw new Error(error.statusText);
    });
}

const customerslist=
    [
        {id: 1,code:"123$45",Name:"John",Email:"Doe",address:"Dhaka",Phone:"0975"},
        {id: 2,code:"123$4567",Name:"John",Email:"Doe",address:"Dhaka",Phone:"0975"}
    ];

export default customerslist;