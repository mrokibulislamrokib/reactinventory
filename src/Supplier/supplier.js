import axios from 'axios';

export function getSingleSupplier(id){
    var request_url='http://localhost:7000/api/supplier/';
    return axios.get(request_url,{
       _id:id
    }).then(function(res){
        if(res.status!==200){
          //  throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
        //throw new Error(error.statusText);
    });
}

export function getSupplier(){
    var request_url='http://localhost:7000/api/supplier/';
    return axios.get(request_url,{
       
    }).then(function(res){
        if(res.status!==200){
      //      throw new Error(res.statusText);
        }else{
            console.log(res.data);
           return res.data;
        }
    }).catch(function(error){
      //  throw new Error(error.statusText);
    });
}

export function addSupplier(supplier){
    console.log(supplier);
    var request_url='http://localhost:7000/api/supplier/';
    return axios.post(request_url,{
        headers: { 
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json',
            'crossDomain': true 
        },
        company_name:supplier.supplier_company_name,
        supplier_name:supplier.supplier_name,
        email:supplier.supplier_email,
        phone:supplier.supplier_phone,
    }).then(function(res){
        if(res.status!==200){
           // throw new Error(res.statusText);
        }else{
            console.log(res.data);
            //return res.data;
        }
    }).catch(function(error){
      //  throw new Error(error.statusText);
    });
}


export function updateSupplier(id,name){
    var request_url='http://localhost:7000/api/supplier/';
    return axios.put(request_url,{
      /*  _id:id,
        company_name:company_name,
        supplier_name:supplier_name,
        email:email,
        phone:phone,
        image:image */
    }).then(function(res){
        if(res.status!==200){
            throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
        throw new Error(error.statusText);
    });
}

export function deleteSupplier(id){
    var request_url='http://localhost:7000/api/supplier/';
    return axios.post(request_url,{
        //_id:id
    }).then(function(res){
        if(res.status!==200){
            throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
        throw new Error(error.statusText);
    });
}



const supplierslist=
    [
        {id:1,Company:"RAHIM AFRUZ",Name:'John',Email:'Doe',Phone:'0967'},
    ];

export default supplierslist;