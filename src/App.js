import React, { Component } from 'react';
import { Router, Route, Switch } from 'react-router';
import Header from './Header';
import Main from './Main';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
        <div>
            <Header/>
            <div id="content" className="content">
              <Main/>
            </div>
        </div>
    );
  }
}

export default App;
