import axios from 'axios';

export function getSingleTax(){
    var request_url='http://localhost:7000/api/tax/';
    return axios.get(request_url,{
     //  _id:id,
    }).then(function(res){
        if(res.status!==200){
            throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
        throw new Error(error.statusText);
    });
}


export function getTax(){
    var request_url='http://localhost:7000/api/tax/';
    return axios.get(request_url,{
       
    }).then(function(res){
        if(res.status!==200){
            throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
        throw new Error(error.statusText);
    });
}

export function addTax(tax){
    var request_url='http://localhost:7000/api/tax/';
    return axios.post(request_url,{
        headers: { 
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json',
            'crossDomain': true 
        },
        tax_code:tax.tax_code,
        tax_name:tax.tax_name,
        tax_percentage:tax.tax_percentage,
        tax_notes:tax.tax_notes
    }).then(function(res){
        if(res.status!==200){
         //   throw new Error(res.statusText);
        }else{
            console.log(res.data);
            //return res.data;
        }
    }).catch(function(error){
        //throw new Error(error.statusText);
    });
}


export function updateTax(id,name){
    var request_url='http://localhost:7000/api/tax/';
    return axios.put(request_url,{
      /*  _id:id,
        tax_code:tax_code,
        tax_name:tax_name,
        tax_percentage:tax_percentage,
        tax_notes:tax_notes */
    }).then(function(res){
        if(res.status!==200){
            throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
        throw new Error(error.statusText);
    });
}

export function deleteTax(id){
    var request_url='http://localhost:7000/api/tax/';
    return axios.post(request_url,{
        _id:id
    }).then(function(res){
        if(res.status!==200){
            throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
        throw new Error(error.statusText);
    });
}

const taxslist=
    [
        {id:1,code:"d010164",Name:'Test',percentage:'5%',Notes:'0967'},
    ];

export default taxslist;