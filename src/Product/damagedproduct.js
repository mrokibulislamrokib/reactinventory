import axios from 'axios';

export function getSingleDamagedProduct(){
    var request_url='http://localhost:7000/api/damagedproduct/';
    return axios.get(request_url,{
       // _id:id
    }).then(function(res){
        if(res.status!==200){
        //    throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
       // throw new Error(error.statusText);
    });
}

export function getDamagedProduct(){
    var request_url='http://localhost:7000/api/damagedproduct/';
    return axios.get(request_url,{
       
    }).then(function(res){
        if(res.status!==200){
        //    throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
      //  throw new Error(error.statusText);
    });
}

export function addDamagedProduct(dproduct){
    var request_url='http://localhost:7000/api/damagedproduct/';
    return axios.post(request_url,{
        //product_id:dproduct.category_name,
        quantity:dproduct.damaged_qty,
        note:dproduct.damaged_note
    }).then(function(res){
        if(res.status!==200){
           // throw new Error(res.statusText);
        }else{
            console.log(res.data);
            return res.data;
        }
    }).catch(function(error){
       // throw new Error(error.statusText);
    });
}


export function updateDamagedProduct(dproduct){
    var request_url='http://localhost:7000/api/damagedproduct/';
    return axios.put(request_url,{
            //_id:id
           // product_id:product_id,
            quantity:dproduct.quantity,
            note:dproduct.note
    }).then(function(res){
        if(res.status!==200){
           // throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
        //throw new Error(error.statusText);
    });
}

export function deleteDamagedProduct(id){
    var request_url='http://localhost:7000/api/damagedproduct/';
    return axios.post(request_url,{
        // _id:id
    }).then(function(res){
        if(res.status!==200){
           // throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
       // throw new Error(error.statusText);
    });
}

const DamagedProductslist=
    [
        {id:1,code:"#5665",Name:"John",Category:"dvf",PurchasePrice:"350",SellingPrice:"456",InStock:"20"},
    ];

export default DamagedProductslist;