import axios from 'axios';

export function getSinglePurchase(){
    var request_url='http://localhost:7000/api/purchase/';
    return axios.get(request_url,{
       //_id:id
    }).then(function(res){
        if(res.status!==200){
      //      throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
     //   throw new Error(error.statusText);
    });
}


export function getPurchase(){
    var request_url='http://localhost:7000/api/purchase/';
    return axios.get(request_url,{
       
    }).then(function(res){
        if(res.status!==200){
        //    throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
       // throw new Error(error.statusText);
    });
}

export function addPurchase(purchase){
    var request_url='http://localhost:7000/api/purchase/';
    return axios.post(request_url,{
        purchase_code:purchase.purchase_code,
     //   supplier_id:purchase.supplier_id,
        purchase_entries:purchase.purchase_entries,
        timestamp:purchase.timestamp 
    }).then(function(res){
        if(res.status!==200){
         //   throw new Error(res.statusText);
        }else{
            console.log(res.data);
          //  return res.data;
        }
    }).catch(function(error){
        //throw new Error(error.statusText);
    });
}


export function updatePurchase(id,name){
    var request_url='http://localhost:7000/api/purchase/';
    return axios.put(request_url,{
        /*
        _id:id
        purchase_code:purchase_code,
        supplier_id:supplier_id,
        purchase_entries:purchase_entries,
        timestamp:timestamp */
    }).then(function(res){
        if(res.status!==200){
            throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
        throw new Error(error.statusText);
    });
}

export function deletePurchase(id){
    var request_url='http://localhost:7000/api/purchase/';
    return axios.post(request_url,{
        // _id:id
    }).then(function(res){
        if(res.status!==200){
            throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
        throw new Error(error.statusText);
    });
}


const purchaselist=
[
    {id:1,code:"#5665",supplier:"John",date:"dvf"},
];

export default purchaselist;