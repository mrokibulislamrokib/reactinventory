   import React, { Component } from 'react';
   import CustomerPost from './customerpost';
   import customerslist, {getSingleCustomer,getCustomer,addCustomer,updateCustomer,deleteCustomer} from './customer';
   
   class  ListCustomer extends Component {

        constructor(props) {
            super(props);
            this.state = {
                isEditing:false,
                _id:'',
                customer_code:'',
                customer_name:'',
                customer_email:'',
                customer_password:'',
                customer_gender:'',
                customer_address:'',
                customer_phone:'',
                customer_discount:'',
                customer_image:'',
                customers: []
                
            };
            this.CustomerSearch=this.CustomerSearch.bind(this);
            this.addCustomer=this.addCustomer.bind(this);
            this.updateCustomer=this.updateCustomer.bind(this);
            this.deleteCustomer=this.deleteCustomer.bind(this);
           // this.editCustomer=this.editCustomer.bind(this);
        }



        customer_code_chanaged(e){
            e.preventDefault();
            this.setState({customer_code:e.target.value});
        }

        customer_name_chanaged(e){
            e.preventDefault();
            this.setState({customer_name:e.target.value});
        }

        customer_email_chanaged(e){
            e.preventDefault();
            this.setState({customer_email:e.target.value});
        }

        customer_password_chanaged(e){
            e.preventDefault();
            this.setState({customer_password:e.target.value});
        }

        customer_gender_chanaged(e){
            e.preventDefault();
            this.setState({customer_gender:e.target.value});
        }

        customer_address_chanaged(e){
            e.preventDefault();
            this.setState({customer_address:e.target.value});
        }

        customer_phone_chanaged(e){
            e.preventDefault();
            this.setState({customer_phone:e.target.value});
        }

        customer_discount_chanaged(e){
            e.preventDefault();
            this.setState({customer_discount:e.target.value});
        }


        componentDidMount(){
            getCustomer().then(response => {
                this.setState({
                    customers: response
                });
            });
        }

        componentWillReceiveProps(){

        }
        

        addCustomer(e){
            
            e.preventDefault();
            
            var customer_code=this.refs.customer_code.value;
            var customer_name=this.refs.customer_name.value;
            var customer_email=this.refs.customer_email.value;
            var customer_password=this.refs.customer_password.value;
            var customer_gender=this.refs.customer_gender.value;
            var customer_address=this.refs.customer_address.value;
            var customer_phone=this.refs.customer_phone.value;
            var customer_discount=this.refs.customer_discount.value;

            var customer={
                customer_code:customer_code,
                customer_name:customer_name,
                customer_email:customer_email,
                customer_password:customer_password,
                customer_gender:customer_gender,
                customer_address:customer_address,
                customer_phone:customer_phone,
                customer_discount:customer_discount
            };

            console.log(customer);

            addCustomer(customer);

           // this.state.customers.push(customer);

          //  this.refs.customerForm.reset();

            console.log('Send this in a POST request:' + customer_name);
        }

        editCustomer(e){
            e.preventDefault();
            var customer_edit_id=this.refs.customer_edit_id.value;
            console.log(customer_edit_id);
        }

        updateCustomer(item,e){
            e.preventDefault();
             
            var customer_code=this.refs.customer_code.value;
            var customer_name=this.refs.customer_name.value;
            var customer_email=this.refs.customer_email.value;
            var customer_password=this.refs.customer_password.value;
            var customer_gender=this.refs.customer_gender.value;
            var customer_address=this.refs.customer_address.value;
            var customer_phone=this.refs.customer_phone.value;
            var customer_discount=this.refs.customer_discount.value;
            
            var customer={
                customer_code:customer_code,
                customer_name:customer_name,
                customer_email:customer_email,
                customer_password:customer_password,
                customer_gender:customer_gender,
                customer_address:customer_address,
                customer_phone:customer_phone,
                customer_discount:customer_discount
            };

            updateCustomer(customer)
  
            //this.refs.customerForm.reset();
  
              console.log('Send this in a POST request:' + customer_name);
          }


        deleteCustomer(e){
           // console.log('Parameter', id);
           let customers=this.state.customers;
          // customers.splice(index);
           //deleteCustomer()
        }

        CustomerSearch(e){
            
        }

        onClick(e){
            e.preventDefault();
            console.log('phone');
        }
/*
        componentDidMount(){
            this.state.customers=customerslist;
        } */


        render() {
          //  console.log(this.state.customers[1])
           return ( 

        <div>
        
        <ol className="breadcrumb pull-right">
            <li><a href="javascript:;">Home</a></li>
            <li className="active">Dashboard</li>
         </ol>
       
       <h1 className="page-header"> Customer List </h1>
       
       <div className ="row">
               <a className="btn btn-success" data-toggle="modal" href='#customer-modal'>Add New Customer</a>

               <a className="btn btn-success" data-toggle="modal" href='#customer_delete'>Add Delete Customer</a>
       </div>

       <br/>
                
       <div className="row">
           <div className="col-md-12">
               <div className="panel panel-inverse">
                   <div className="panel-heading">
                       <div className="panel-heading-btn">
                           <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i className="fa fa-expand"></i></a>
                           <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i className="fa fa-repeat"></i></a>
                           <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i className="fa fa-minus"></i></a>
                           <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i className="fa fa-times"></i></a>
                       </div>
                       <h4 className ="panel-title">Data Table - Default</h4>
                   </div>
                   <div className ="panel-body">
                      
                       <table id="data-table" className="table table-striped table-bordered">
                           <thead>
                               <tr>
                                    <th>#</th>
                                    <th>Code</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th>Phone</th>
                                    <th>Options</th>
                               </tr>
                           </thead>
                           <tbody>
                                {
                                   this.state.customers.map(function(item, i){
                                       return  <CustomerPost key={item._id}{...item}/>
                                   })
                                }
                           </tbody>
                       </table>
                   </div>
               </div>
           </div>
       </div> 

<div className="modal fade" id="customer-modal">
    <div className="modal-dialog">
        <div className="modal-content">
            <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 className="modal-title">Modal title</h4>
            </div>
            <div className="modal-body">
                <form onSubmit={this.addCustomer} ref="customerForm" className="form-horizontal form-bordered" encType="application/x-www-form-urlencoded"   method="post" acceptCharset="utf-8">
                    <div className="form-group">
                        <label className="control-label col-md-4 col-sm-4">
                            Customer Code </label>
                        <div className="col-md-6 col-sm-6">
                            <input className="form-control" type="text" ref="customer_code" onChange={this.customer_code_chanaged} name="customer_code" required="" placeholder="" value="" readOnly=""/>
                        </div>
                    </div>

                    <div className="form-group">
                        <label className="control-label col-md-4 col-sm-4">
                            Customer Name </label>
                        <div className="col-md-6 col-sm-6">
                            <input className="form-control" type="text" ref="customer_name" name="name" required="" placeholder="Customer Name"/>
                        </div>
                    </div>

                    <div className="form-group">
                        <label className="control-label col-md-4 col-sm-4">
                            Email </label>
                        <div className="col-md-6 col-sm-6">
                            <input className="form-control" type="email" ref="customer_email" name="email" required="" placeholder="Customer Email"/>
                        </div>
                    </div>

                    <div className="form-group">
                        <label className="control-label col-md-4 col-sm-4">
                            Password </label>
                        <div className="col-md-6 col-sm-6">
                            <input className="form-control" type="password" ref="customer_password" name="password" required="" placeholder="Customer Password"/>
                        </div>
                    </div>

                    <div className="form-group">
                        <label className="control-label col-md-4 col-sm-4">
                            Gender </label>
                        <div className="col-md-6 col-sm-6">
                            <select className="form-control selectpicker" ref="customer_gender" data-size="10" data-live-search="true" data-style="btn-white" name="gender">
                                <option value="" selected="">Select Gender</option>
                                <option value="1">Male</option>
                                <option value="2">Female</option>
                            </select>
                        </div>
                    </div>

                    <div className="form-group">
                        <label className="control-label col-md-4 col-sm-4">
                            Address </label>
                        <div className="col-md-6 col-sm-6">
                            <textarea className="form-control" name="address" ref="customer_address" placeholder="Customer Address" rows="3"></textarea>
                        </div>
                    </div>

                    <div className="form-group">
                        <label className="control-label col-md-4 col-sm-4">
                            Phone </label>
                        <div className="col-md-6 col-sm-6">
                            <input className="form-control" type="text" ref="customer_phone" name="phone" placeholder="Customer Phone Number"/>
                        </div>
                    </div>

                    <div className="form-group">
                        <label className="control-label col-md-4 col-sm-4">
                            Discount </label>
                        <div className="col-md-6 col-sm-6">
                            <input className="form-control" type="text" ref="customer_discount" name="discount_percentage" required="" placeholder="Customer Discount Percentage"/>
                        </div>
                    </div>

                    <div className="form-group">
                        <label className="control-label col-md-4 col-sm-4">
                            Image </label>
                        <div className="col-md-6 col-sm-6">
                            <input className="form-control" type="file" ref="customer_image" name="userfile" placeholder="Customer Image"/>
                        </div>
                    </div>

                    <div className="form-group">
                        <label className="control-label col-md-4 col-sm-4"></label>
                        <div className="col-md-6 col-sm-6">
                            <button type="submit" className="btn btn-success">Save Customer</button>
                        </div>
                    </div>

                </form>
                
            </div>
            <div className="modal-footer">
                <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" className="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>


    </div>);
        }

    }
   
   export default ListCustomer;
