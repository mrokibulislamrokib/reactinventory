import React, { Component } from 'react';

class ProfileSetting extends Component {
    
    constructor(props){
        super(props);
        this.state={
            profile_image:'',
            profile_name:'',
            profile_email:'',
            profile_previous_password:'',
            profile_new_password:'',
            profile_confirm_password:''
        }
        this.updateProfileSetting=this.updateProfileSetting.bind(this);
        this.updatePassword=this.updatePassword.bind(this);
    }

    updateProfileSetting(e){
        e.preventDefault();
        var profile_name=this.refs.profile_name.value;
        var profile_email=this.refs.profile_email.value;

        var profile={
            profile_name:profile_name,
            profile_email:profile_email
        }
    }

    updatePassword(e){
        e.preventDefault();
        var profile_previous_password=this.refs.profile_previous_password.value;
        var profile_new_password=this.profile_new_password.value;
        var profile_confirm_password=this.profile_confirm_password.value;

        var profile={
            profile_new_password:profile_new_password
        }
    }
    
    render() {
        return (
            <div>
                <div className="row">
                    <div className="col-md-6">
                        <div className="panel panel-inverse">
                            <div className="panel-heading">
                                <div className="panel-heading-btn">
                                    <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i className="fa fa-expand"></i></a>
                                    <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i className="fa fa-minus"></i></a>
                                </div>
                                <h4 className="panel-title"> Basic Information </h4>
                            </div>
                            <div className="panel-body">
                            <form onSubmit={this.updateProfileSetting} className="form-horizontal form-bordered" data-parsley-validate="true" encType="multipart/form-data" method="post" acceptCharset="utf-8" noValidate="">
                            <div className="form-group">
                                <label className="control-label col-md-3 col-sm-3">
                                    Image                            </label>
                                <div className="col-md-9 col-sm-9">
                                    <img src=""/>
                                </div>
                            </div>
    
                            <div className="form-group">
                                <label className="control-label col-md-3 col-sm-3">Name</label>
                                <div className="col-md-9 col-sm-9">
                                    <input className="form-control" type="text" name="name" ref="profile_name"
                                    data-parsley-required="true" placeholder="Your Name" defaultValue="Manirul Isalm" data-parsley-id="5119"/>
                                    <ul className="parsley-errors-list" id="parsley-id-5119"></ul>
                                </div>
                            </div>
    
                            <div className="form-group">
                                <label className="control-label col-md-3 col-sm-3">Email</label>
                                <div className="col-md-9 col-sm-9">
                                    <input className="form-control" type="text" name="email" ref="profile_email"
                                    data-parsley-required="true" placeholder="Your Email" defaultValue="admin@email.com" data-parsley-id="6582"/>
                                    <ul className="parsley-errors-list" id="parsley-id-6582"></ul>
                                </div>
                            </div>
    
                            <div className="form-group">
                                <label className="control-label col-md-3 col-sm-3">Image</label>
                                <div className="col-md-9 col-sm-9">
                                    <input className="form-control" type="file" ref="profile_image" name="userfile" placeholder="Admin Image" data-parsley-id="3398"/>
                                    <ul className="parsley-errors-list" id="parsley-id-3398"></ul>
                                </div>
                            </div>
    
                            <div className="form-group">
                                <label className="control-label col-md-3 col-sm-3"></label>
                                <div className="col-md-9 col-sm-9">
                                    <button type="submit" className="btn btn-success">Update</button>
                                </div>
                            </div>
    
                        </form>               
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="panel panel-inverse">
                            <div className="panel-heading">
                                <div className="panel-heading-btn">
                                    <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i className="fa fa-expand"></i></a>
                                    <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i className="fa fa-minus"></i></a>
                                </div>
                                <h4 className="panel-title">Change Password</h4>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={this.updatePassword} className="form-horizontal form-bordered"  encType="multipart/form-data" method="post" acceptCharset="utf-8">
                                    
                                    <div className="form-group">
                                        <label className="control-label col-md-5 col-sm-5">Current Password</label>
                                        <div className="col-md-7 col-sm-7">
                                            <input className="form-control" type="password" ref="profile_previous_password" name="previous_password" data-parsley-required="true"
                                                placeholder="Your Present Password" />
                                        </div>
                                    </div>

                                    <div className="form-group">
                                        <label className="control-label col-md-5 col-sm-5">
                                            New Password                            </label>
                                        <div className="col-md-7 col-sm-7">
                                            <input className="form-control" type="password" ref="profile_new_password" name="new_password" data-parsley-required="true"
                                                placeholder="Your New Password" />
                                        </div>
                                    </div>

                                    <div className="form-group">
                                        <label className="control-label col-md-5 col-sm-5">
                                            Confirm New Password                            </label>
                                        <div className="col-md-7 col-sm-7">
                                            <input className="form-control" type="password" name="profile_confirm_password" data-parsley-required="true"
                                                placeholder="Confirm New Password" />
                                        </div>
                                    </div>

                                    <div className="form-group">
                                        <label className="control-label col-md-5 col-sm-5"></label>
                                        <div className="col-md-7 col-sm-7">
                                            <button type="submit" className="btn btn-success">Update</button>
                                        </div>
                                    </div>

                                </form>                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ProfileSetting;