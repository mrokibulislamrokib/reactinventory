import React, { Component } from 'react';
import taxslist,{getTax,addTax,updateTax,deleteTax} from './tax';
class ListTax extends Component {
    
    constructor(props) {
        super(props);
        this.state={
            isEditing:false,
            isadding:false,
            isdeleting:false,
            tax_code:'',
            tax_name:'',
            tax_percentage:'',
            tax_notes:'',
            taxs:[]
        }
        this.addTax=this.addTax.bind(this);
        this.updateTax=this.updateTax.bind(this);
        this.deleteTax=this.deleteTax.bind(this);
        this.TaxSearch=this.TaxSearch.bind(this);
    }

    componentDidMount(){
        getTax().then(response => {
            this.setState({
                taxs: response
            });
        });
    }

    componentWillReceiveProps(){

    }

    addTax(e){
       e.preventDefault();
       var tax_code=this.refs.tax_code.value;
       var tax_name=this.refs.tax_name.value;
       var tax_percentage=this.refs.tax_percentage.value;
       var tax_notes=this.refs.tax_percentage.value;
       var tax={
        tax_code:tax_code,
        tax_name:tax_name,
        tax_percentage:tax_percentage,
        tax_notes:tax_notes
       }
       addTax(tax)
       //this.state.taxs.push(tax);
       console.log('Send this in a POST request:' + tax_code);
    }

    updateTax(e){
        e.preventDefault();
        this.setState({
            isEditing:true
        });
      //  document.getElementById('tax-modal').show();
        var tax_code=this.refs.tax_code.value;
        var tax_name=this.refs.tax_name.value;
        var tax_percentage=this.refs.tax_percentage.value;
        var tax_notes=this.refs.tax_percentage.value;
        console.log('Send this in a POST request:' + tax_code);
    }

    deleteTax(){
        this.setState({
            isdeleting:true
        });
    }

    TaxSearch(e){
        e.preventDefault();
        var search=this.refs.customerSearch.value;
    }

    render() {
        return (
            <div>
            <ol className="breadcrumb pull-right">
                <li><a href="javascript:;">Home</a></li>
                <li className="active">Dashboard</li>
            </ol>

            <h1 className="page-header">Dashboard <small>header small text goes here...</small></h1>

            <div className="row">
                    <a className="btn btn-success" data-toggle="modal" href='#tax-modal'>Add New Tax</a>
            </div>

            <br/>

                <div className="row">
                    <div className="col-md-12">
                        <div className="panel panel-inverse">
                            <div className="panel-heading">
                                <div className="panel-heading-btn">
                                    <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i className="fa fa-expand"></i></a>
                                    <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i className="fa fa-repeat"></i></a>
                                    <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i className="fa fa-minus"></i></a>
                                    <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i className="fa fa-times"></i></a>
                                </div>
                                <h4 className="panel-title">Data Table - Default</h4>
                            </div>
                            <div className="panel-body">
                                <form className="form-inline pull-right" onSubmit={this.TaxSearch}>
                                    <input type="text" ref="customerSearch" className="form-control"  />
                                    <button type="submit" className="btn btn-primary">Search</button>
                                </form>
                                
                                <table id="data-table" className="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>id</th>
                                            <th>Code</th>
                                            <th>Name</th>
                                            <th>Percentage</th>
                                            <th>Notes</th>
                                            <th>Options </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            this.state.taxs.map(function(item,i){
                                                return <tr className="odd gradeX" key={i}>
                                                            <td>{item._id}</td>
                                                            <td>{item.tax_code}</td>
                                                            <td>{item.tax_ame}</td>
                                                            <td>{item.tax_percentage}</td>
                                                            <td>{item.tax_notes}</td>
                                                            <td> 
                                                                <button className="btn btn-info btn-icon btn-circle btn-sm">
                                                                    <i className="fa fa-info"></i>
                                                                </button>
                                        
                                                                <button className="btn btn-success btn-icon btn-circle btn-sm">
                                                                    <i className="fa fa-edit"></i>
                                                                </button>
                                        
                                                                <button   className="btn btn-danger btn-icon btn-circle btn-sm">
                                                                    <i className="fa fa-trash"></i>
                                                                </button>
                                                            </td>
                                                        </tr>
                                            })
                                        }
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

<div className="modal fade" id="tax-modal">
    <div className="modal-dialog">
        <div className="modal-content">
            
            <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 className="modal-title">Add Customer</h4>
            </div>
            
            <div className="modal-body">

            		<form onSubmit={this.addTax} className="form-horizontal form-bordered" encType="application/x-www-form-urlencoded"   method="post" acceptCharset="utf-8">
                        
                        <div className="form-group">
                            <label className="control-label col-md-4 col-sm-4"> Tax Code</label>
                            <div className="col-md-6 col-sm-6">
                                <input className="form-control" type="text" ref="tax_code" name="tax_code" required="" defaultValue="3088aae"/>
                            </div>
                        </div>

                        <div className="form-group">
                            <label className="control-label col-md-4 col-sm-4">Tax Name	</label>
                            <div className="col-md-6 col-sm-6">
                                <input className="form-control" type="text" ref="tax_name" name="name" required="" placeholder="Tax Name"/>
                            </div>
                        </div>

                        <div className="form-group">
                            <label className="control-label col-md-4 col-sm-4">Tax Percentage</label>
                            <div className="col-md-6 col-sm-6">
                                <input className="form-control" type="text" ref="tax_percentage" name="percentage" required="" placeholder="Tax Percentage"/>
                            </div>
                        </div>

                        <div className="form-group">
                            <label className="control-label col-md-4 col-sm-4">Notes</label>
                            <div className="col-md-6 col-sm-6">
                                <textarea className="form-control" ref="tax_notes" name="notes" placeholder="Notes" rows="3"></textarea>
                            </div>
                        </div>

                        <div className="form-group">
                            <label className="control-label col-md-4 col-sm-4"></label>
                            <div className="col-md-6 col-sm-6">
                                <button type="submit" className="btn btn-success">Save Tax Settings</button>
                            </div>
                        </div>

					</form>             
            </div>

            <div className="modal-footer">
                <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" className="btn btn-primary">Save changes</button>
            </div>

        </div>
    </div>
</div>
            </div>

        );
    }

}

export default ListTax;