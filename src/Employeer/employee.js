import axios from 'axios';


export function getSingleEmployee(){
    var request_url='localhost:7000/api/employee/';
    return axios.get(request_url,{
       // _id:id
    }).then(function(res){
        if(res.status!==200){
         //   throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
       // throw new Error(error.statusText);
    });
}

export function getEmployee(){
    var request_url='localhost:7000/api/employee/';
    return axios.get(request_url,{}).then(function(res){
        if(res.status!==200){
        //    throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
       // throw new Error(error.statusText);
    });
}


export function addEmployee(employee){
    var request_url='localhost:7000/api/employee/';
    return axios.post(request_url,{
        headers: { 
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json',
            'crossDomain': true 
        },
        employee_name:employee.employee_name,
        email:employee.employee_name,
        employee_type:employee.employee_type,
        address:employee.employee_address,
        phone:employee.employee_phone,
    }).then(function(res){
        if(res.status!==200){
        //    throw new Error(res.statusText);
        }else{
            console.log(res.data);
           // return res.data;
        }
    }).catch(function(error){
       // throw new Error(error.statusText);
    });
}


export function updateEmployee(id,name){
    var request_url='localhost:7000/api/employee/';
    return axios.put(request_url,{
        /* 
        _id:id
        employee_name:employee_name,
        email:email,
        employee_type:employee_type,
        address:address,
        phone:phone,
        image:image */
    }).then(function(res){
        if(res.status!==200){
        ///    throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
       // throw new Error(error.statusText);
    });
}

export function deleteEmployee(id){
    var request_url='localhost:7000/api/employee/';
    return axios.post(request_url,{
        // _id:id
    }).then(function(res){
        if(res.status!==200){
       //     throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
       // throw new Error(error.statusText);
    });
}


const employeeslist=
    [
        {id:1,Name:"John",Email:"Doe",Address:"",Phone:"",Type:"",}, 
    ];

export default employeeslist;