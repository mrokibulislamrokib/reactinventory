import React, { Component } from 'react';
import settinglist,{getSetting,addSetting,updateSetting} from './set';

class Setting extends Component {
    
    constructor(props){
        super(props);
        this.state={
            setting_name:'',
            setting_email:'',
            setting_address:'',
            setting_phone:'',
            setting_currency:'',
            settings:settinglist
        };
        this.updateSetting=this.updateSetting.bind(this);
    }


    updateSetting(e){
        e.preventDefault();
        var setting_name=this.refs.setting_name.value;
        var setting_email=this.refs.setting_email.value;
        var setting_address=this.refs.setting_address.value;
        var setting_phone=this.refs.setting_phone.value;
        var setting_currency=this.refs.setting_currency.value;

        var setting={
            setting_name:setting_name,
            setting_email:setting_email,
            setting_address:setting_address,
            setting_phone:setting_phone,
            setting_currency:setting_currency
        }

        addSetting(setting);

       // this.state.settings.push(setting);
        
        console.log('Send this in a POST request:' + setting_name);
   
    }
    

    updateSetting(e){
        e.preventDefault();
        var setting_name=this.refs.setting_name.value;
        var setting_email=this.refs.setting_email.value;
        var setting_address=this.refs.setting_address.value;
        var setting_phone=this.refs.setting_phone.value;
        var setting_currency=this.refs.setting_currency.value;

        var setting={
            setting_name:setting_name,
            setting_email:setting_email,
            setting_address:setting_address,
            setting_phone:setting_phone,
            setting_currency:setting_currency
        }

        updateSetting(setting);

       // this.state.settings.push(setting);
        
        console.log('Send this in a POST request:' + setting_name);
   
    }
    
    render() {
        return (
        
        <div>
            <ol className="breadcrumb pull-right">
                <li><a href="javascript:;">Home</a></li>
                <li className="active">Dashboard</li>
            </ol>
	
            <h1 className="page-header">System Settings</h1>

            <div className="row">
                    <div className="col-md-8 ui-sortable">
                        <div className="panel panel-inverse">
                            <div className="panel-heading">
                                <div className="panel-heading-btn">
                                    <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i className="fa fa-expand"></i></a>
                                    <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i className="fa fa-minus"></i></a>
                                </div>
                                <h4 className="panel-title">General Information</h4>
                            </div>

                            <div className="panel-body">
                            <form onSubmit={this.updateSetting} className="form-horizontal form-bordered" data-parsley-validate="true" encType="multipart/form-data" method="post" acceptCharset="utf-8" noValidate="">
                                    {
                                        this.state.settings.map(function(){
                                            
                                        })
                                    }
                                    <div className="form-group">
                                        <label className="control-label col-md-3 col-sm-3">Company Name</label>
                                        <div className="col-md-9 col-sm-9">
                                            <input className="form-control" type="text" ref="setting_name" name="company_name" data-parsley-required="true" placeholder="Company Name"/>
                                                <ul className="parsley-errors-list"></ul>
                                        </div>
                                    </div>

                                    <div className="form-group">
                                        <label className="control-label col-md-3 col-sm-3">Company Email</label>
                                        <div className="col-md-9 col-sm-9">
                                            <input className="form-control" type="text" ref="setting_email" name="company_email" data-parsley-required="true" placeholder="Company Email"/>
                                            <ul className="parsley-errors-list"></ul>
                                        </div>
                                    </div>

                                    <div className="form-group">
                                        <label className="control-label col-md-3 col-sm-3">Address</label>
                                        <div className="col-md-9 col-sm-9">
                                            <input className="form-control" type="text"  ref="setting_address" name="address" placeholder="Address" defaultValue="Bangladesh" data-parsley-id="9253"/>
                                            <ul className="parsley-errors-list"></ul>
                                        </div>
                                    </div>

                                    <div className="form-group">
                                        <label className="control-label col-md-3 col-sm-3">Phone</label>
                                        <div className="col-md-9 col-sm-9">
                                            <input className="form-control" type="text" ref="setting_phone" name="phone" placeholder="Phone" defaultValue="01859343274" data-parsley-id="3283"/>
                                            <ul className="parsley-errors-list"></ul>
                                        </div>
                                    </div>

                                    <div className="form-group">
                                        <label className="control-label col-md-3 col-sm-3">Currency</label>
                                        <div className="col-md-9 col-sm-9">
                                            <input className="form-control" type="text" ref="setting_currency" name="currency" placeholder="Currency" defaultValue="TK" data-parsley-id="6097"/>
                                            <ul className="parsley-errors-list"></ul>
                                        </div>
                                    </div>

                                    <div className="form-group">
                                        <label className="control-label col-md-3 col-sm-3"></label>
                                        <div className="col-md-9 col-sm-9">
                                            <button type="submit" className="btn btn-success">Update</button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>

                    <div className="col-md-4 ui-sortable">
                        <div className="panel panel-inverse">
                            <div className="panel-heading">
                                <div className="panel-heading-btn">
                                    <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i className="fa fa-expand"></i></a>
                                    <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i className="fa fa-minus"></i></a>
                                </div>
                                <h4 className="panel-title">Update Logo</h4>
                            </div>
                            <div className="panel-body">
                                <form action="" className="form-horizontal" encType="multipart/form-data" method="post" acceptCharset="utf-8">
                                    <div className="form-group">
                                        <div className="col-md-12 col-sm-12">
                                            <img  src=""/>
                                        </div>
                                    </div>

                                    <div className="form-group">
                                        <div className="col-md-12 col-sm-12">
                                            <input className="form-control" type="file" name="userfile" placeholder="System Logo"/>
                                        </div>
                                    </div>

                                    <div className="form-group">
                                        <div className="col-md-6 col-sm-6">
                                            <button type="submit" className="btn btn-success">Update</button>
                                        </div>
                                    </div>

                                </form> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Setting;