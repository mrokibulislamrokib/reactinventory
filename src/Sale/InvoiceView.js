import React, { Component } from 'react';
import {getSetting} from '../Setting/set';
import {getPurchase} from '../Purchase/purchase';
import {getSale} from '../Sale/sale';
import {getProduct} from '../Product/product';
import {getCustomer} from '../Customer/customer';
import {getSupplier} from '../Supplier/supplier';

class InvoiceView extends Component {

    constructor(props){
        super(props);
        this.state={
            settings:[],
            customers:[],
            products:[],
            invoices:[]
        }
    }

    componentDidMount() {
        getSetting().then(response => {
            this.setState({
                settings: response
            });
        });

        getCustomer().then(response => {
            this.setState({
                customers: response
            });
        });

        getProduct().then(response => {
            this.setState({
                products: response
            });
        });

        getSale().then(response => {
            this.setState({
                invoices: response
            });
        });
    }
    
    render() {
        return (

<div>
    <div className="invoice">
        <div className="invoice-company">
            <span className="pull-right hidden-print">
                <a href="javascript:;" onclick="window.print()" className="btn btn-sm btn-success m-b-10"><i className="fa fa-print m-r-5"></i> Print</a>
            </span>
            Ezzitech        
        </div>
            
        <div className="invoice-header">
            <div className="invoice-from">
                <small>From</small>
                <address className="m-t-5 m-b-5">
                    {
                        this.state.settings.map(function(item,i){

                        })
                    }
                    <strong>Ezzitech</strong><br/>
                    Bangladesh<br/>
                    Phone: 01859343274<br/>
                    Email: info@ezzitech.com                
                </address>
            </div>
            
            <div className="invoice-to">
                <small>To</small>
                <address className="m-t-5 m-b-5">
                    <strong>Walk In Clients</strong><br/>
                    {
                        this.state.customers.map(function(item,i){
                            
                        })
                    }
                    Address: Random<br/>
                    Phone: 56662621<br/>
                    Email: customer@email.com                
                </address>
            </div>

            <div className="invoice-date">
                <div className="date m-t-5">18th Nov, 2017</div>
                <div className="invoice-detail">
                    <strong>Invoice Code: ef7a6bf64f</strong><br/>
                    <strong>Payment Method: Cash</strong>
                </div>
            </div>
        </div>

        <div className="invoice-content">
            <div className="table-responsive">
                <table className="table table-invoice table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Serial No</th>
                            <th>Products</th>
                            <th>Unit Price</th>
                            <th>Quantity</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>b912b9777c</td>
                            <td>Shirt-123</td>
                            <td>TK 850</td>
                            <td>1</td>
                            <td>TK 850</td>
                        </tr>
                                        
                    </tbody>

                    <tbody>
                    
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><strong>Grand Total</strong></td>
                            <td className="text-right">TK 425.00</td>
                        </tr>
                                        
                    </tbody>

                </table>
            </div>
            <div className="table-responsive">
                <table className="table table-invoice table-bordered">
                    <thead>
                        <tr>
                            <th>Payments</th>
                            <th>Date</th>
                            <th>Amount</th>
                            <th>Payment Method</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>18th Nov, 2017</td>
                            <td>TK 425</td>
                            <td>Cash</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div className="invoice-price">
                <div className="invoice-price-left">
                    <div className="invoice-price-row">
                        <div className="sub-price">
                            <small>Sub Total</small>
                            TK 850                        </div>
                        <div className="sub-price">
                            <i className="fa fa-plus"></i>
                        </div>
                        <div className="sub-price">
                             % VAT
                        </div>
                        <div className="sub-price">
                            <i className="fa fa-minus"></i>
                        </div>
                        <div className="sub-price">
                            50 % Discount                        </div>
                    </div>
                </div>
                <div className="invoice-price-right">
                    <small>Total Paid</small>
                    TK 425                </div>
            </div>
        </div>
        <div className="invoice-footer text-muted">
            <p className="text-center m-b-5">
                Thank You For Your Business            </p>
            <p className="text-center">
                <span className="m-r-10"><i className="fa fa-globe"></i> Ezzitech</span>
                <span className="m-r-10"><i className="fa fa-phone"></i> 01859343274</span>
                <span className="m-r-10"><i className="fa fa-envelope"></i> info@ezzitech.com</span>
            </p>
        </div>
    </div>
                
            </div>
        );
    }
}

export default InvoiceView;