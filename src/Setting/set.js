import axios from 'axios';

export function getSetting(){
    var request_url='http://localhost:7000/api/setting/';
    return axios.get(request_url,{
       
    }).then(function(res){
        if(res.status!==200){
            throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
        throw new Error(error.statusText);
    });
}

export function addSetting(id,name){
    var request_url='http://localhost:7000/api/setting/';
    return axios.put(request_url,{
       // setting_type:setting_type,
       // setting_description:setting_description
    }).then(function(res){
        if(res.status!==200){
            throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
        throw new Error(error.statusText);
    });
}


export function updateSetting(id,name){
    var request_url='http://localhost:7000/api/setting/';
    return axios.put(request_url,{
       // _id:id,
       // setting_type:setting_type,
        //setting_description:setting_description
    }).then(function(res){
        if(res.status!==200){
            throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
        throw new Error(error.statusText);
    });
}


const settinglist=
    [
        {name:1,email:"d010164",address:'Test',phone:'5%',currency:'0967'},
    ];

export default settinglist;