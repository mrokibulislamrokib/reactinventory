import React, { Component } from 'react';

class ListProductBarcode extends Component {
    
    constructor(props) {
        super(props);
        this.setState={};
        this.ProductBarcodeSearch=this.ProductBarcodeSearch.bind(this);
    }

    componentDidMount(){

    }

    componentWillReceiveProps(){

    }

    ProductBarcodeSearch(){

    }

    render() {
        return (
            <div>   
                <ol className="breadcrumb pull-right">
                    <li><a href="javascript:;">Home</a></li>
                    <li className="active">Dashboard</li>
                </ol>
                <h1 className="page-header">Dashboard <small>header small text goes here...</small></h1>
                
                <div className="row">
                    <div className="col-md-12">
                        <div className="panel panel-inverse">
                            <div className="panel-heading">
                                <div className="panel-heading-btn">
                                    <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i className="fa fa-expand"></i></a>
                                    <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i className="fa fa-repeat"></i></a>
                                    <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i className="fa fa-minus"></i></a>
                                    <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i className="fa fa-times"></i></a>
                                </div>
                                <h4 className="panel-title">Data Table - Default</h4>
                            </div>
                            <div className="panel-body">
                                <table id="data-table" className="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Code</th>
                                            <th>Name</th>
                                            <th>BarCode</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr className="odd gradeX">
                                            <td> </td>
                                            <td> </td>
                                            <td> </td>
                                            <td> </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ListProductBarcode;