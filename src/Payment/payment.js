import axios from 'axios';

export function getPaymentByType(){
    var request_url='http://localhost:7000/api/customer/';
    return axios.get(request_url,{
        // _id:id,
        //type:type
    }).then(function(res){
        if(res.status!==200){
            throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
        throw new Error(error.statusText);
    });
}

export function addPayment(payment){
    var request_url='http://localhost:7000/api/customer/';
    return axios.post(request_url,{
       /*
        type:type,
        method:method,
        amount:amount,
        timestamp:timestamp,
        supplier_id:supplier_id,
        customer_id:customer_id,
        invoice_id:invoice_id,
        purchase_id:purchase_id,
        order_id:order_id
       */
    }).then(function(res){
        if(res.status!==200){
            throw new Error(res.statusText);
        }else{
            return res.data;
        }
    }).catch(function(error){
        throw new Error(error.statusText);
    });
}

