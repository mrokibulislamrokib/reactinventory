import React, { Component } from 'react';
import subcategorieslist,{getSubCategory,addSubCategory,updateSubCategory,deleteSubCategory} from './subcategory';
import {getCategory} from './category';
class ListProductSubCategory extends Component {
    
    constructor(props) {
        super(props);
        this.state={
            sub_category_name:'',
            category_name:'',
            sub_category_description:'',
            subcategories:[],
            categories:[]
        }

        this.addSubCategory=this.addSubCategory.bind(this);
        this.updateSubCategory=this.updateSubCategory.bind(this);
        this.deleteSubCategory=this.deleteSubCategory.bind(this);
        this.ProductSubCategorySearch=this.ProductSubCategorySearch.bind(this);
    }

    componentDidMount(){
        //this.state.subcategories=subcategorieslist;
        getSubCategory().then(response => {
            this.setState({
                subcategories: response
            });
        });

        getCategory().then(response => {
            this.setState({
                categories: response
            });
        });
       // console.log(getCategory());
       //  var cat=getCategory();
       // console.log(cat);
    }

    componentWillReceiveProps(){
        
    }

    addSubCategory(e){
        e.preventDefault();
        var sub_category_name=this.refs.sub_category_name.value;
        var sub_category_description=this.refs.category_name;
        var sub_category_description=this.refs.sub_category_description;

        var subcategory={
            sub_category_name:sub_category_name,
            sub_category_description:sub_category_description
        }

        addSubCategory(subcategory);

      //  this.state.subcategories.push(subcategory);

        console.log('Send this in a POST request:' + sub_category_name);
    }

    updateSubCategory(e){
        e.preventDefault();
        var sub_category_name=this.refs.sub_category_name.value;
        var sub_category_description=this.refs.category_name;
        var sub_category_description=this.refs.sub_category_description;
        console.log('Send this in a POST request:' + sub_category_name);
    }

    deleteSubCategory(){

    }

    ProductSubCategorySearch(){

    }

    render() {
        return (
            <div>
                <ol className="breadcrumb pull-right">
                    <li><a href="javascript:;">Home</a></li>
                    <li className="active">Dashboard</li>
                </ol>

                <h1 className="page-header">Dashboard <small>header small text goes here...</small></h1>
                            
                <div className="row">
                    <a className="btn btn-success" data-toggle="modal" href='#SubCategorie-modal'>Add New Sub Categorie</a>
                </div>

                <br/>

                <div className="row">
                    <div className="col-md-12">
                        <div className="panel panel-inverse">
                            <div className="panel-heading">
                                <div className="panel-heading-btn">
                                    <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i className="fa fa-expand"></i></a>
                                    <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i className="fa fa-repeat"></i></a>
                                    <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i className="fa fa-minus"></i></a>
                                    <a href="javascript:;" className="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i className="fa fa-times"></i></a>
                                </div>
                                <h4 className="panel-title">Data Table - Default</h4>
                            </div>
                            <div className="panel-body">
                                <table id="data-table" className="table table-striped table-bordered">
                                        <thead>
                                        <tr>    
                                            <th> #</th>
                                            <th> Name</th>
                                            <th> Category </th>
                                            <th> Description</th>
                                            <th> Options</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            this.state.subcategories.map(function(item,i){

                                             return     <tr className="odd gradeX" key={i}>
                                                            <td> {item.id}      </td>
                                                            <td> {item.name}    </td>
                                                            <td> {item.category} </td>
                                                            <td> {item.description}</td>
                                                            <td> </td>
                                                        </tr>

                                            })
                                        }
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>


                <div className="modal fade" id="SubCategorie-modal">
    <div className="modal-dialog">
        <div className="modal-content">
            <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 className="modal-title">Modal title</h4>
            </div>
            <div className="modal-body">

            	<form onSubmit={this.addSubCategory} className="form-horizontal form-bordered">

				   <div className="form-group">
				      <label className="control-label col-md-4 col-sm-4">
				      Name		</label>
				      <div className="col-md-6 col-sm-6">
				         <input className="form-control" type="text" ref="sub_category_name" name="name" required="" placeholder="Name"/>
				      </div>
				   </div>
   
				   <div className="form-group">
				      <label className="control-label col-md-4 col-sm-4"> Category</label>
				      <div className="col-md-6 col-sm-6">
                         <select className="form-control selectpicker" data-size="10" data-live-search="true" data-style="btn-white" 
                            ref="category_name" name="category_id" required="">
				            <option value="" selected="">Select Product Category</option>
                                    {
                                        this.state.categories.map(function(item, i){
                                            return <option value="">{item.name} </option>
                                        })
                                    }
				         </select>
				      </div>
				   </div>


				   <div className="form-group">
				      <label className="control-label col-md-4 col-sm-4">
				      Description        </label>
				      <div className="col-md-6 col-sm-6">
				         <textarea className="form-control" ref="sub_category_description" name="description" placeholder="Sub Category Description" rows="3"></textarea>
				      </div>
				   </div>

				   <div className="form-group">
				      <label className="control-label col-md-4 col-sm-4"></label>
				      <div className="col-md-6 col-sm-6">
				         <button type="submit" className="btn btn-success">Save Sub Category</button>
				      </div>
				   </div>

				</form>               
            </div>
            <div className="modal-footer">
                <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" className="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
            </div>
        );
    }
}

export default ListProductSubCategory;